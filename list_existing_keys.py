import os


def get_filenames_and_labels(data_folder, test_or_train='test'):
    keys = list(os.listdir(data_folder + ('%s-resized256/' % (test_or_train))))
    keys.sort()

    # print(keys)
    with open('keys_for_%s_2018new' % test_or_train, "w") as fout:
        fout.write(" ".join([str(el) for el in keys]))


get_filenames_and_labels(data_folder='/data/landmarks2019-data/2018/', test_or_train='test')
get_filenames_and_labels(data_folder='/data/landmarks2019-data/2018/', test_or_train='index')
