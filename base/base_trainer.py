import json
import logging
import math
import os

import numpy as np
import torch
import torch.optim as optim
import visdom

from model.loss import CenterLoss, RingLoss, SoftmaxLoss
from utils.util import ensure_dir


class BaseTrainer:
    """
    Base class for all trainers
    """

    def __init__(self, model, loss, metrics, resume, config,
                 train_loader, test_loader, train_logger=None):
        self.config = config
        self.batch_size = train_loader.batch_size
        self.train_loader = train_loader
        self.test_loader = test_loader

        self.valid = self.test_loader is not None

        self.log_step = np.ceil(np.sqrt(self.batch_size))

        self.logger = logging.getLogger(self.__class__.__name__)
        self.model = model
        self.loss = loss

        self.metrics = metrics
        self.name = config['name']
        self.epochs = config['trainer']['epochs']
        self.save_freq = config['trainer']['save_freq']
        self.verbosity = config['trainer']['verbosity']
        self.with_cuda = config['cuda'] and torch.cuda.is_available()
        if config['cuda'] and not torch.cuda.is_available():
            self.logger.warning('Warning: There\'s no CUDA support on this machine, '
                                'training is performed on CPU.')
        else:
            self.gpu = torch.device('cuda:' + str(config['gpu']))
            self.model = self.model.to(self.gpu)

        self.train_logger = train_logger
        # here we add to the optimizer only those parameters that are not frozen!
        non_frozen_parameters = [p for p in model.parameters() if p.requires_grad]
        print('%d non_frozen_parameters ' % len(non_frozen_parameters))

        self.center_loss = RingLoss()  # CenterLoss(num_classes=14951, feat_dim=256, use_gpu=True)
        # non_frozen_parameters.extend(list(self.center_loss.parameters()))
        self.softmax_loss = SoftmaxLoss(input_size=256, output_size=14951, normalize=True, loss_weight=1.0)



        self.optimizer = getattr(optim, config['optimizer_type'])(
            non_frozen_parameters,
            **config['optimizer']
        )
        print('self.optimizer ', self.optimizer)
        self.lr_scheduler = getattr(optim.lr_scheduler,
                                    config['lr_scheduler_type'], None)
        if self.lr_scheduler:
            self.lr_scheduler = self.lr_scheduler(self.optimizer, **config['lr_scheduler'])
            self.lr_scheduler_freq = config['lr_scheduler_freq']
        self.monitor = config['trainer']['monitor']
        self.monitor_mode = config['trainer']['monitor_mode']
        assert self.monitor_mode == 'min' or self.monitor_mode == 'max'
        self.monitor_best = math.inf if self.monitor_mode == 'min' else -math.inf
        self.start_epoch = 1
        self.checkpoint_dir = os.path.join(config['trainer']['save_dir'], self.name)
        ensure_dir(self.checkpoint_dir)
        json.dump(config, open(os.path.join(self.checkpoint_dir, 'config.json'), 'w'),
                  indent=4, sort_keys=False)
        if resume:
            self._resume_checkpoint(resume)

        # initialization for visualization
        # self.vis = visdom.Visdom(env='main %s' % self.train_loader.dataset.data_folder[-15:])
        # self.vis.close()
        # self.loss_plot = self.vis.line(Y=np.zeros(1), X=np.zeros(1))
        self.r_loss = []
        self.iterations = []
        self.loss_name = ''

    def _to_tensor(self, data, target):
        data, target = torch.FloatTensor(data), torch.LongTensor(target)
        if self.with_cuda:
            data, target = data.to(self.gpu), target.to(self.gpu)
        return data, target

    def train(self):
        """
        Full training logic
        """
        print('Let\'s start train method')
        for epoch in range(self.start_epoch, self.epochs + 1):
            result = self._train_epoch(epoch)
            log = {'epoch': epoch}
            for key, value in result.items():
                if key == 'metrics':
                    for i, metric in enumerate(self.metrics):
                        log[metric.__name__] = result['metrics'][i]
                elif key == 'val_metrics':
                    for i, metric in enumerate(self.metrics):
                        log['val_' + metric.__name__] = result['val_metrics'][i]
                else:
                    log[key] = value
            if self.train_logger is not None:
                self.train_logger.add_entry(log)
                if self.verbosity >= 1:
                    for key, value in log.items():
                        self.logger.info('    {:15s}: {}'.format(str(key), value))
            # if (self.monitor_mode == 'min' and log[self.monitor] < self.monitor_best) \
            #         or (self.monitor_mode == 'max' and log[self.monitor] > self.monitor_best):
            #     self.monitor_best = log[self.monitor]
            #     self._save_checkpoint(epoch, log, save_best=True)
            if epoch % self.save_freq == 0:
                self._save_checkpoint(epoch, log)
            if self.lr_scheduler and epoch % self.lr_scheduler_freq == 0:
                self.lr_scheduler.step(epoch)
                lr = self.lr_scheduler.get_lr()[0]
                self.logger.info('New Learning Rate: {:.6f}'.format(lr))
            with torch.cuda.device(self.config['gpu']):
                torch.cuda.empty_cache()

    def _train_epoch(self, epoch):
        """
        Training logic for an epoch

        :param epoch: Current epoch number
        """
        raise NotImplementedError

    def _save_checkpoint(self, epoch, log, save_best=False):
        """
        Saving checkpoints

        :param epoch: current epoch number
        :param log: logging information of the epoch
        :param save_best: if True, rename the saved checkpoint to 'model_best.pth.tar'
        """
        arch = type(self.model).__name__
        state = {
            'arch': arch,
            'epoch': epoch,
            'logger': self.train_logger,
            'state_dict': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict(),
            'monitor_best': self.monitor_best,
            'config': self.config
        }
        filename = os.path.join(self.checkpoint_dir, 'checkpoint-epoch{:03d}-loss-{:.4f}.pth.tar'
                                .format(epoch, log['loss']))
        torch.save(state, filename)
        if save_best:
            os.rename(filename, os.path.join(self.checkpoint_dir, 'model_best.pth.tar'))
            self.logger.info('Saving current best: {} ...'.format('model_best.pth.tar'))
        else:
            self.logger.info('Saving checkpoint: {} ...'.format(filename))

    def _resume_checkpoint(self, resume_path):
        """
        Resume from saved checkpoints

        :param resume_path: Checkpoint path to be resumed
        """
        self.logger.info('Loading checkpoint: {} ...'.format(resume_path))
        checkpoint = torch.load(resume_path)
        self.start_epoch = checkpoint['epoch'] + 1
        self.monitor_best = checkpoint['monitor_best']
        self.model.load_state_dict(checkpoint['state_dict'])

        # self.optimizer.load_state_dict(checkpoint['optimizer'])

        if self.with_cuda:
            for state in self.optimizer.state.values():
                for k, v in state.items():
                    if isinstance(v, torch.Tensor):
                        state[k] = v.cuda(self.gpu)
        self.train_logger = checkpoint['logger']
        self.config = checkpoint['config']
        self.logger.info('Checkpoint \'{}\' (epoch {}) loaded'.format(resume_path, self.start_epoch))
