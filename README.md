# Landmark2019



**How to run:**

If you run it for the first time:

`train.py -c config.json`

If you want to resume learning using the best model so far

`train.py -c config.json -r saved/<classification_or_representation>/Landmark2019/model_best.pth.tar`

**Details about dataset structure**




**Details of the retrieval training:**

Retrieval consists of the following parts:
1. Pre-train a convolutional network on classification task for a few epochs.
Here we use ResNet-50 pretrained on ImageNet with frozen first 3 layers. 
The corresponding code is in `trainer/classification_trainer.py`.
![Screenshot](pictures/classification_pretraining.png)

2. Remove the classification tail and get the global descriptors (for example SPoC descriptors of size 256).
The codeforthe SPoC layer is in `model/modules/spoc_layer.py`. 
![Screenshot](pictures/representation_network_inference.png)

3. Tune these global descriptors using retrieval-specific loss (for example histogram loss) and special sampling strategies.
The corresponding code is in `trainer/representation_trainer.py`.
The code for retrieval-specific loss functions is in `model/loss.py`.
The code for special sampling strategies are in `data_loader/sampling.py` and `trainer/representation_trainer.py`.
![Screenshot](pictures/representation_network_training.png)

**Details of the classification using the retrieval result:**

Give a retrieval result we can perform classification in a different ways:

- knn classification with k = 1 (`results/knn.py`)
- use query expansion/reranking (`improvement/query_expansion.py`, `improvement/reranking.py`) in order to improve 
the retrieval result and apply a classifier after such an improvement 
- map the retrieval result to some reasonable features and train a classifier  