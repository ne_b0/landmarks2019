from torch.autograd import Variable

from data_loader.sampling import get_indices_for_equal_sampling
from utils import utilstriplet
from utils.util import get_signs_matrix
import matplotlib.pyplot as plt

from utils.utilstriplet import random_hard_negative
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch.autograd import Function
import numpy as np


class HistogramLoss(torch.nn.Module):
    """
           Evgeniya Ustinova, Victor Lempitsky
           \textit{Learning Deep Embeddings with Histogram Loss}.
           2016
    https://arxiv.org/abs/1611.00822
    """

    def __init__(self, num_steps, config, use_gpu=True):
        super(HistogramLoss, self).__init__()
        self.config = config
        self.step = 2 / (num_steps - 1)  # step size
        self.use_gpu = use_gpu
        self.t = torch.range(-1, 1, self.step).view(-1, 1)
        self.t_size = self.t.size()[0]
        if self.use_gpu:
            self.t = self.t.cuda(torch.device('cuda:' + str(config['gpu'])))

    def histogram(self, indices, size, s):
        s_repeat = s.repeat(self.t_size, 1)
        delta_repeat = (torch.floor((s_repeat.data + 1) / self.step) * self.step - 1).float()
        indsa = (delta_repeat == (self.t - self.step)) & indices
        indsb = (delta_repeat == self.t) & indices
        s_repeat[~(indsb | indsa)] = 0
        s_repeat[indsa] = (s_repeat - Variable(self.t) + self.step)[indsa] / self.step
        s_repeat[indsb] = (-s_repeat + Variable(self.t) + self.step)[indsb] / self.step
        return s_repeat.sum(1) / size.float()

    def forward(self, input_1, input_2, target_1, target_2):
        with torch.cuda.device(self.config['gpu']):
            target_size = target_1.size()[0]

            signs_matrix = (target_1.repeat(target_size, 1) == target_2.view(-1, 1).repeat(1, target_size)).data

            cosine_similarities = torch.mm(input_1, input_2.transpose(0, 1))

            # we want to take just upper triangle of the all pairs matrix
            # to compute the loss
            # because pairs in the lower triangle are the same

            if self.config['sampling']['strategy'] == 'equal':
                s_indices = torch.from_numpy(
                    get_indices_for_equal_sampling(self.config, signs_matrix, negative_pair_sign=0)).byte()
            else:
                s_indices = torch.triu(torch.ones(cosine_similarities.size()), 1).byte()

            positive_indices = signs_matrix[s_indices].repeat(self.t_size, 1)
            negative_indices = ~signs_matrix[s_indices].repeat(self.t_size, 1)

            positive_size = signs_matrix[s_indices].sum()
            negative_size = (~signs_matrix[s_indices]).sum()

            s = cosine_similarities[s_indices].view(1, -1)  # unroll cosine_similarities in a row

            histogram_positive = self.histogram(positive_indices, positive_size, s)
            histogram_negative = self.histogram(negative_indices, negative_size, s)
            histogram_positive_repeat = histogram_positive.view(-1, 1).repeat(1, histogram_positive.size()[0])
            histogram_positive_indices = torch.tril(torch.ones(histogram_positive_repeat.size()), -1).byte()

            histogram_positive_repeat[histogram_positive_indices] = 0
            histogram_positive_cdf = histogram_positive_repeat.sum(0)

            # constant = 0.1
            # additional_penalty = constant * torch.nonzero(histogram_negative).shape[0]
            # print('additional_penalty ', additional_penalty)
            # input()

            loss = torch.sum(histogram_negative * histogram_positive_cdf)  # + additional_penalty

            return loss


def histogram_loss(input_1, input_2, target_1, target_2, config):
    # check_dimensionality_consistence(input_1, input_2, target_1, target_2)

    # It is essential to do L2-normalization before using histogram loss
    # because it works with direct scalar product
    # so let's do smoke-check
    inputs_number = input_1.shape[0]
    index_to_test = np.random.randint(low=0, high=inputs_number)
    assert torch.abs(torch.norm(input_1[index_to_test]) - 1.0) < 10e-5, \
        'Histogram loss should work with normalized data! ' \
        'But torch.norm(input_1[%d]) = %.10e %s' % (index_to_test, torch.norm(input_1[index_to_test]), input_1)

    loss = HistogramLoss(num_steps=config['loss']['steps_number_for_histogram'], config=config)
    draw_cosine_similarities_histograms(input_1, input_2, target_1, target_2, config)
    return loss(input_1, input_2, target_1, target_2)


def cosine_loss(input_1, input_2, target_1, target_2, config):
    with torch.cuda.device(config['gpu']):
        # check_dimensionality_consistence(input_1, input_2, target_1, target_2)

        batch_size = input_1.shape[0]
        dimension = input_1.shape[1]

        draw_cosine_similarities_histograms(input_1, input_2, target_1, target_2, config)

        loss = torch.nn.CosineEmbeddingLoss(margin=config['loss']['margin_for_cosine_loss']).cuda()

        input_1 = input_1.expand(batch_size, batch_size, dimension)
        input_2 = torch.transpose(input_2.expand(batch_size, batch_size, dimension), 0, 1)

        signs_matrix = get_signs_matrix(config, target_1, target_2, negative_sign=-1) \
            .contiguous().view(batch_size, batch_size)

        if config['sampling']['strategy'] == 'equal':
            indices_for_loss = get_indices_for_equal_sampling(config, signs_matrix, negative_pair_sign=-1)
            signs_matrix = signs_matrix[indices_for_loss].cuda().float()
            input_1 = input_1[indices_for_loss]
            input_2 = input_2[indices_for_loss]

        # unroll matrices to use them in loss function
        signs_matrix = signs_matrix.view(-1, 1).float().cuda()
        input_1 = input_1.contiguous().view(-1, dimension)
        input_2 = input_2.contiguous().view(-1, dimension)

        return loss(input_1, input_2, signs_matrix)


def draw_cosine_similarities_histograms(input_1, input_2, target_1, target_2, config):
    if config['draw_histograms']:  # and target_1[0] == 7:
        batch_size = input_1.size(0)
        signs_matrix = get_signs_matrix(config, target_1, target_2, negative_sign=-1)

        cosines_neg = []
        cosines_pos = []
        for i in range(batch_size):
            for j in range(batch_size):
                cosine_numpy = torch.dot(input_1[i], input_2[j]).cpu().data.numpy()
                if signs_matrix[i, j] == 1:
                    cosines_pos.append(cosine_numpy)
                else:
                    cosines_neg.append(cosine_numpy)

        cosines_pos = np.array(cosines_pos)
        cosines_neg = np.array(cosines_neg)

        plt.hist(cosines_pos, 100, density=True, facecolor='g', alpha=0.75)
        plt.xlabel('cosine')
        plt.ylabel('pairs number')
        plt.hist(cosines_neg, 100, density=True, facecolor='r', alpha=0.5)
        plt.title('Histogram of positive/negative cosine similarities for %s ' % config['loss']['name'])
        plt.grid(True)
        plt.show()
        plt.close()


def check_dimensionality_consistence(input_1, input_2, target_1, target_2):
    assert input_1.shape == input_2.shape, 'Your inputs should have the same shape, ' \
                                           'but actual: \n input_1.shape = %s,' \
                                           '\n input_2.shape = %s' \
                                           '\n Possible solution: change your batch_size in config ' \
                                           'to be a divisor of the dataset size' % (input_1.shape, input_2.shape)
    assert target_1.shape == target_2.shape, 'Your targets should have the same shape, ' \
                                             'but actual: \n target_1.shape = %s,' \
                                             '\n target_2.shape = %s' % (target_1.shape, target_2.shape)
    assert input_1.shape[0] == target_1.shape[0], 'Your input and target should have the same batch size, ' \
                                                  'but actual: \n input_1.batch_size = %s,' \
                                                  '\n target_1.batch_size = %s' % (input_1.size(0), target_1.size(0))


def classification_loss(inp, target, config):
    loss = torch.nn.CrossEntropyLoss()
    # print('inp ', inp.shape, ' target ', target.shape)
    return loss(inp, target)


def triplet_loss(input_1, input_2, target_1, target_2, config):
    with torch.cuda.device(config['gpu']):
        # check_dimensionality_consistence(input_1, input_2, target_1, target_2)

        batch_size = input_1.shape[0]
        dimension = input_1.shape[1]

        margin = 0.5

        triplet_selector = utilstriplet.FunctionNegativeTripletSelector(margin=margin,
                                                                        negative_selection_fn=random_hard_negative,
                                                                        cpu=False)

        loss = torch.nn.TripletMarginLoss(margin=margin)

        triplets = triplet_selector.get_triplets(input_1, target_1)
        # print('triplets ', triplets)

        anchors = input_1[triplets[0]]
        positives = input_1[triplets[1]]
        negatives = input_1[triplets[2]]

        return loss(anchors, positives, negatives)


class ContrastiveLoss(torch.nn.Module):
    """
    Contrastive loss function.
    Based on:
    """

    def __init__(self, margin=1.0):
        super(ContrastiveLoss, self).__init__()
        self.margin = margin

    def check_type_forward(self, in_types):
        assert len(in_types) == 3

        x0_type, x1_type, y_type = in_types

        # print('x0_type, x1_type, y_type ', x0_type.shape, x1_type.shape, y_type.shape)

        assert x0_type.size() == x1_type.shape
        assert x1_type.size()[0] == y_type.shape[0]
        assert x1_type.size()[0] > 0
        assert x0_type.dim() == 2
        assert x1_type.dim() == 2
        assert y_type.dim() == 1

    def forward(self, x0, x1, y):
        self.check_type_forward((x0, x1, y))

        # euclidian distance
        diff = x0 - x1
        dist_sq = torch.sum(torch.pow(diff, 2), 1)
        dist = torch.sqrt(dist_sq)

        mdist = self.margin - dist
        dist = torch.clamp(mdist, min=0.0)
        loss = y * dist_sq + (1 - y) * torch.pow(dist, 2)
        loss = torch.sum(loss) / 2.0 / x0.size()[0]
        return loss


def contrastive_loss(input_1, input_2, target_1, target_2, config):
    with torch.cuda.device(config['gpu']):
        # check_dimensionality_consistence(input_1, input_2, target_1, target_2)

        batch_size = input_1.shape[0]
        dimension = input_1.shape[1]

        loss = ContrastiveLoss(margin=config['loss']['margin_for_cosine_loss']).cuda()

        input_1 = input_1.expand(batch_size, batch_size, dimension)
        input_2 = torch.transpose(input_2.expand(batch_size, batch_size, dimension), 0, 1)

        # from LeCun paper Dimensionality Reduction by Learning an Invariant Mapping
        # http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
        #
        # Y = 0 if X~1 and X~2 are deemed similar, and Y = 1 if they are deemed dissimilar
        signs_matrix = get_signs_matrix(config, target_1, target_2, negative_sign=1, positive_sign=0) \
            .contiguous().view(batch_size, batch_size)

        # unroll matrices to use them in loss function
        signs_matrix = signs_matrix.view(-1, 1).flatten().float().cuda()
        input_1 = input_1.contiguous().view(-1, dimension)
        input_2 = input_2.contiguous().view(-1, dimension)

    return loss(input_1, input_2, signs_matrix)


class CenterLoss(torch.nn.Module):
    """Center loss.

    Reference:
    Wen et al. A Discriminative Feature Learning Approach for Deep Face Recognition. ECCV 2016.

    Args:
        num_classes (int): number of classes.
        feat_dim (int): feature dimension.
    """

    def __init__(self, num_classes=10, feat_dim=2, use_gpu=True):
        super(CenterLoss, self).__init__()
        self.num_classes = num_classes
        self.feat_dim = feat_dim
        self.use_gpu = use_gpu

        if self.use_gpu:
            self.centers = torch.nn.Parameter(torch.randn(self.num_classes, self.feat_dim).cuda())
        else:
            self.centers = torch.nn.Parameter(torch.randn(self.num_classes, self.feat_dim))

    def forward(self, x, labels):
        """
        Args:
            x: feature matrix with shape (batch_size, feat_dim).
            labels: ground truth labels with shape (batch_size).
        """
        batch_size = x.size(0)
        distmat = torch.pow(x, 2).sum(dim=1, keepdim=True).expand(batch_size, self.num_classes) + \
                  torch.pow(self.centers, 2).sum(dim=1, keepdim=True).expand(self.num_classes, batch_size).t()
        distmat.addmm_(1, -2, x, self.centers.t())

        classes = torch.arange(self.num_classes).long()
        if self.use_gpu: classes = classes.cuda()
        labels = labels.unsqueeze(1).expand(batch_size, self.num_classes)
        mask = labels.eq(classes.expand(batch_size, self.num_classes))

        dist = distmat * mask.float()
        loss = dist.clamp(min=1e-12, max=1e+12).sum() / batch_size

        return loss


def center_loss(input_1, input_2, target_1, target_2, config):
    with torch.cuda.device(config['gpu']):
        # check_dimensionality_consistence(input_1, input_2, target_1, target_2)

        batch_size = input_1.shape[0]
        dimension = input_1.shape[1]

        loss = CenterLoss(num_classes=14951, feat_dim=256, use_gpu=True)

    return loss(input_1, target_1)


class PhiKernel(Function):
    def __init__(self, m, lamb):
        self.mlambda = [
            lambda x: x ** 0,
            lambda x: x,
            lambda x: 2 * x ** 2 - 1,
            lambda x: 4 * x ** 3 - 3 * x,
            lambda x: 8 * x ** 4 - 8 * x ** 2 + 1,
            lambda x: 16 * x ** 5 - 20 * x ** 3 + 5 * x
        ]
        self.mcoeff_w = [
            lambda x: 0,
            lambda x: 1,
            lambda x: 4 * x,
            lambda x: 12 * x ** 2 - 3,
            lambda x: 32 * x ** 3 - 16 * x,
            lambda x: 80 * x ** 4 - 60 * x ** 2 + 5
        ]
        self.mcoeff_x = [
            lambda x: -1,
            lambda x: 0,
            lambda x: 2 * x ** 2 + 1,
            lambda x: 8 * x ** 3,
            lambda x: 24 * x ** 4 - 8 * x ** 2 - 1,
            lambda x: 64 * x ** 5 - 40 * x ** 3
        ]
        self.m = m
        self.lamb = lamb

    def forward(self, input, weight, label):
        xlen = input.pow(2).sum(1).pow(0.5).view(-1, 1)  # size=B
        wlen = weight.pow(2).sum(1).pow(0.5).view(1, -1)
        cos_theta = (input.mm(weight.t()) / xlen / wlen).clamp(-1, 1)
        cos_m_theta = self.mlambda[self.m](cos_theta)

        k = (self.m * cos_theta.acos() / np.pi).floor()
        phi_theta = (-1) ** k * cos_m_theta - 2 * k

        feat = cos_theta * xlen
        phi_theta = phi_theta * xlen

        index = (feat * 0.0).scatter_(1, label.view(-1, 1), 1).byte()
        feat[index] -= feat[index] / (1.0 + self.lamb)
        feat[index] += phi_theta[index] / (1.0 + self.lamb)
        self.save_for_backward(input, weight, label)
        self.cos_theta = (cos_theta * index.float()).sum(dim=1).view(-1, 1)
        self.k = (k * index.float()).sum(dim=1).view(-1, 1)
        self.xlen, self.index = xlen, index
        return feat

    def backward(self, grad_outputs):
        input, weight, label = self.saved_variables
        input, weight, label = input.data, weight.data, label.data
        grad_input = grad_weight = None
        grad_input = grad_outputs.mm(weight) * self.lamb / (1.0 + self.lamb)
        grad_outputs_label = (grad_outputs * self.index.float()).sum(dim=1).view(-1, 1)
        coeff_w = ((-1) ** self.k) * self.mcoeff_w[self.m](self.cos_theta)
        coeff_x = (((-1) ** self.k) * self.mcoeff_x[self.m](self.cos_theta) + 2 * self.k) / self.xlen
        coeff_norm = (coeff_w.pow(2) + coeff_x.pow(2)).pow(0.5)
        grad_input += grad_outputs_label * (coeff_w / coeff_norm) * torch.index_select(weight, 0, label) / (
                    1.0 + self.lamb)
        grad_input -= grad_outputs_label * (coeff_x / coeff_norm) * input / (1.0 + self.lamb)
        grad_input += (grad_outputs * (1 - self.index).float()).mm(weight) / (1.0 + self.lamb)
        grad_weight = grad_outputs.t().mm(input)
        return grad_input, grad_weight, None


class SoftmaxLoss(torch.nn.Module):
    def __init__(self, input_size, output_size, normalize=True, loss_weight=1.0):
        super(SoftmaxLoss, self).__init__()
        self.fc = torch.nn.Linear(input_size, int(output_size), bias=False).cuda()
        torch.nn.init.kaiming_uniform_(self.fc.weight, 0.25).cuda()
        self.weight = self.fc.weight.cuda()
        self.loss_weight = loss_weight
        self.normalize = normalize

    def forward(self, x, y):
        if self.normalize:
            self.fc.weight.renorm(2, 0, 1e-5).mul(1e5).cuda()
        prob = F.log_softmax(self.fc(x).cuda(), dim=1).cuda()
        self.prob = prob
        loss = F.nll_loss(prob, y)
        return loss.mul_(self.loss_weight)


class RingLoss(nn.Module):
    def __init__(self, type='auto', loss_weight=1.0):
        """
        :param type: type of loss ('l1', 'l2', 'auto')
        :param loss_weight: weight of loss, for 'l1' and 'l2', try with 0.01. For 'auto', try with 1.0.
        :return:
        """
        super(RingLoss, self).__init__()

        self.radius = Parameter(torch.Tensor(1))
        self.radius.data.fill_(-1)
        self.loss_weight = loss_weight
        self.type = type

    def forward(self, x):
        x = x.cuda()
        x = x.pow(2).sum(dim=1).pow(0.5)
        if self.radius.data[0] < 0:  # Initialize the radius with the mean feature norm of first iteration
            self.radius.data.fill_(x.mean().data)
        if self.type == 'l1':  # Smooth L1 Loss
            loss1 = F.smooth_l1_loss(x, self.radius.expand_as(x)).mul_(self.loss_weight)
            loss2 = F.smooth_l1_loss(self.radius.expand_as(x), x).mul_(self.loss_weight)
            ringloss = loss1 + loss2
        elif self.type == 'auto':  # Divide the L2 Loss by the feature's own norm
            diff = x.sub(self.radius.expand_as(x).cuda()) / (x.mean().detach().clamp(min=0.5).cuda())
            diff_sq = torch.pow(torch.abs(diff), 2).mean()
            ringloss = diff_sq.mul_(self.loss_weight)
        else:  # L2 Loss, if not specified
            diff = x.sub(self.radius.expand_as(x))
            diff_sq = torch.pow(torch.abs(diff), 2).mean()
            ringloss = diff_sq.mul_(self.loss_weight)
        return ringloss


class AngleSoftmax(nn.Module):
    def __init__(self, input_size, output_size, normalize=True, m=4, lambda_max=1000.0, lambda_min=5.0,
                 power=1.0, gamma=0.1, loss_weight=1.0):
        """
        :param input_size: Input channel size.
        :param output_size: Number of Class.
        :param normalize: Whether do weight normalization.
        :param m: An integer, specifying the margin type, take value of [0,1,2,3,4,5].
        :param lambda_max: Starting value for lambda.
        :param lambda_min: Minimum value for lambda.
        :param power: Decreasing strategy for lambda.
        :param gamma: Decreasing strategy for lambda.
        :param loss_weight: Loss weight for this loss.
        """
        super(AngleSoftmax, self).__init__()
        self.loss_weight = loss_weight
        self.normalize = normalize
        self.weight = Parameter(torch.Tensor(int(output_size), input_size))
        nn.init.kaiming_uniform_(self.weight, 1.0)
        self.m = m

        self.it = 0
        self.LambdaMin = lambda_min
        self.LambdaMax = lambda_max
        self.gamma = gamma
        self.power = power

    def forward(self, x, y):

        if self.normalize:
            wl = self.weight.pow(2).sum(1).pow(0.5)
            wn = self.weight / wl.view(-1, 1)
            self.weight.data.copy_(wn.data)
        if self.training:
            lamb = max(self.LambdaMin, self.LambdaMax / (1 + self.gamma * self.it) ** self.power)
            self.it += 1
            phi_kernel = PhiKernel(self.m, lamb)
            feat = phi_kernel(x, self.weight, y)
            loss = F.nll_loss(F.log_softmax(feat, dim=1), y)
        else:
            feat = x.mm(self.weight.t())
            self.prob = F.log_softmax(feat, dim=1)
            loss = F.nll_loss(self.prob, y)

        return loss.mul_(self.loss_weight)
