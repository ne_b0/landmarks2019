import os

import numpy as np
import torch
from sklearn.decomposition import PCA
from torch import nn as nn
from torch.nn import Parameter
from torch.nn.functional import avg_pool2d

from model.modules.l2_normalization import L2Normalization
from model.modules.rmac import rmac
from utils.util import outputs_for_large_dataset, read_inference_results_from_disk


# The paper about SPoC descriptors
# Artem Babenko, Victor Lempitsky.
#         \textit{Aggregating Deep Convolutional Features for Image Retrieval}.
#         2015
# https://arxiv.org/pdf/1510.07493.pdf

def learn_pca_matrix_for_spocs_with_sklearn(spocs, desired_dimension):
    pca = PCA(n_components=desired_dimension)
    u, s, v = pca._fit(torch.t(spocs).cpu().numpy())
    return u[:, :desired_dimension], s[:desired_dimension]


def create_initial_pca_matrix_for_dataset(train_loader, desired_dimension, network):
    config = train_loader.config
    if 'initial_PCA_matrix.npy' not in os.listdir(config['temp_folder']):
        pca_matrix, singular_values = create_new_pca_matrix_for_dataset(desired_dimension, network, train_loader)
    else:
        pca_matrix, singular_values = download_existing_pca_matrix(config['temp_folder'])
    return pca_matrix, singular_values


def download_existing_pca_matrix(temp_folder):
    pca_matrix_filename = os.path.join(temp_folder, 'initial_PCA_matrix.npy')
    print('Downloading initial PCA matrix from %s' % pca_matrix_filename)
    pca_matrix = np.load(pca_matrix_filename)
    singular_values = np.load(os.path.join(temp_folder, 'initial_singular_values.npy'))
    return pca_matrix, singular_values


def create_new_pca_matrix_for_dataset(desired_dimension: int, network, train_loader):
    config = train_loader.config
    print('Creating initial PCA matrix')
    spoc_before_pca = SpocBeforeDimensionReduction()

    # result_dict = outputs_for_large_dataset(train_loader, network)
    #
    # all_outputs = result_dict['all_outputs']

    # This is code for super large dataset to create a pca matrix using only part of all batches
    # after the inference is already done and stored on the disk
    # here we just read as many batches as we can
    path = os.path.join(config['temp_folder'], 'train', '')
    result_dict = read_inference_results_from_disk(config, batches_number=2700, name='train')
    all_outputs = result_dict['all_outputs']

    all_outputs = spoc_before_pca(all_outputs)
    pca_matrix, singular_values = learn_pca_matrix_for_spocs_with_sklearn(all_outputs.data, desired_dimension)
    np.save(os.path.join(config['temp_folder'],
                         'initial_PCA_matrix'), pca_matrix)
    np.save(os.path.join(config['temp_folder'],
                         'initial_singular_values'), singular_values)
    return pca_matrix, singular_values


class SpocBeforeDimensionReduction(nn.Module):
    def __init__(self):
        super(SpocBeforeDimensionReduction, self).__init__()
        # L2 - normalization
        self.normalization_before_dimension_reduction = L2Normalization()

        # for gem
        p = 3
        self.p = Parameter(torch.ones(1) * p)
        self.eps = 1e-6


    def forward(self, inp):
        batch_size = inp.size(0)
        dimension = inp.size(1)
        # sum pooling SPOC descriptors
        # descriptors = torch.sum(inp.view(batch_size, dimension, -1), dim=2)

        # rmacs descriptors
        # descriptors = rmac(inp)

        # gem descriptors
        descriptors = avg_pool2d(inp.clamp(min=self.eps).pow(self.p), (inp.size(-2), inp.size(-1))).pow(1. / self.p)

        # normalization
        descriptors = self.normalization_before_dimension_reduction(descriptors)
        return descriptors

    def __repr__(self):
        return self.__class__.__name__


class Spoc(nn.Module):
    def __init__(self, initial_pca_matrix, initial_singular_values):
        super(Spoc, self).__init__()
        self.spoc_before_dimension_reduction = SpocBeforeDimensionReduction()
        # PCA
        self.pca_matrix = nn.Parameter(initial_pca_matrix, requires_grad=True)
        self.singular_values = nn.Parameter(initial_singular_values, requires_grad=True)
        # L2 - normalization
        self.normalization_after_dimension_reduction = L2Normalization()

    def forward(self, inp):
        # before PCA
        spocs = self.spoc_before_dimension_reduction(inp)
        # PCA
        spocs = torch.div(torch.mm(spocs, self.pca_matrix), self.singular_values)
        # normalization
        # we need this normalization step in order to use representation-specific losses
        spocs = self.normalization_after_dimension_reduction(spocs)

        return spocs

    def __repr__(self):
        return self.__class__.__name__
