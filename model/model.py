import torch
import torch.nn as nn
from torch.nn import Linear

from base.base_model import BaseModel
from model.modules.spoc_layer import Spoc, create_initial_pca_matrix_for_dataset
from model.radenovich import init_network


class Landmark2019Model(BaseModel):
    def __init__(self, config, loader_for_pca_initialization, basic_net):
        """
        here we cut off this tail of Resnet in order to replace it with the Spoc layer
            (avgpool): AvgPool2d(kernel_size=7, stride=1, padding=0)
            (fc): Linear(in_features=2048, out_features=1000, bias=True)
        Actually it's not ideally clear which layer is better to take,
        but we should start somewhere

        I took the idea about Resnet from this article, but combined it with SPoCs
        Albert Gordo, Jon Almazan, Jerome Revaud, Diane Larlus.
        \textit{Deep Image Retrieval: Learning global representations for image search}.
        2016
        """
        super(Landmark2019Model, self).__init__(config)
        self.config = config

        if self.config['model_type'] == 'radenovich':
            net_params = {}
            net_params['architecture'] = 'resnet101'
            net_params['pooling'] = 'gem'
            net_params['local_whitening'] = True
            net_params['regional'] = True
            net_params['whitening'] = True
            net_params['pretrained'] = True

            self.net = init_network(net_params).cuda(torch.device('cuda:' + str(config['gpu'])))

        else:
            self.desired_dimension = self.config['model']['desired_embedding_dimension']

            basic_net = basic_net.cuda(torch.device('cuda:' + str(config['gpu'])))

            # for resnet50
            # self.representation_network = nn.Sequential(*list(basic_net.children())[:8])

            # for se_resnext50_32x4d
            self.representation_network = nn.Sequential(*list(basic_net.children())[:6])
            basic_net = None
            # for densenet
            #           print('list(basic_net.features.children()) ', list(basic_net.features.children()))
            #            self.representation_network = nn.Sequential(*list(basic_net.features.children()))
            #           print('self.representation_network ', self.representation_network)

            self.train_loader = loader_for_pca_initialization
            initial_pca_matrix, initial_singular_values = create_initial_pca_matrix_for_dataset(
                loader_for_pca_initialization,
                self.desired_dimension,
                self.representation_network
            )

            self.spoc_layer = Spoc(
                torch.from_numpy(initial_pca_matrix).cuda(torch.device('cuda:' + str(config['gpu']))),
                torch.from_numpy(initial_singular_values).cuda(torch.device('cuda:' + str(config['gpu'])))
            )

            if self.config['2head']:
                self.fc = Linear(in_features=self.desired_dimension, out_features=14951)

    def forward(self, x):
        if self.config['2head']:
            descriptors = self.representation_network(x)
            descriptors = self.spoc_layer(descriptors)
            for_softmax = None  # self.fc(descriptors)
            return descriptors, for_softmax

        if self.config['model_type'] == 'radenovich':
            x = self.net(x)
        else:
            x = self.representation_network(x)
            x = self.spoc_layer(x)


        return x
