import csv
import json
import os

import numpy as np
import torchvision.transforms as transforms
from PIL import Image, ImageDraw, ImageFont
from torch.utils.data import Dataset


class Landmark2019(Dataset):
    def __init__(self, data_folder, config, name):
        print('data_folder ', data_folder, ' name ', name)
        self.data_folder = data_folder
        self.name = name
        self.config = config

        initial_image_size = config['data_loader']['initial_image_size']
        if name == 'train':
            self.transform = transforms.Compose([
                # transforms.Resize((initial_image_size, initial_image_size)),
                transforms.RandomCrop(config['data_loader']['initial_crop_size'], padding=0),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
            ])
        else:
            self.transform = transforms.Compose([
                # transforms.Resize((initial_image_size, initial_image_size)),
                transforms.ToTensor(),
            ])
        self.transform_for_visualization = transforms.ToTensor()
        self.classes_number = 0
        self.path_labels_csv = os.path.join(self.config['data_loader']['data_dir'], '%s.csv' % self.name)
        self.images, self.labels = self.get_images_and_labels()

    def __len__(self):
        if self.name == 'train':
            return 100
        return self.images.shape[0]

    def __getitem__(self, index):
        try:
            image = self.transform(Image.open(self.images[index]).convert('RGB'))
            label = self.labels[index]
            return image, label, index
        except:
            print('Image %s is not downloaded yet!' % self.images[index])

    def get_image_and_label_for_visualization(self, index):
        label = self.labels[index]
        if os.path.exists(self.images[index]):
            try:
                image = Image.open(self.images[index]).convert('RGB')
                image = image.resize(
                    (self.config['visualization']['image_size'], self.config['visualization']['image_size']))
                draw = ImageDraw.Draw(image)
                font = ImageFont.truetype(font='Junicode.ttf', size=30)
                draw.text((0, 0), str(label), (255, 0, 0), font=font)
                image = self.transform_for_visualization(image)
            except:
                print('self.images[index] !!!!', self.images[index])
        else:
            print('Image %s is not downloaded yet!', self.images[index])

        return image, label

    def get_images_and_labels(self):
        images = []
        labels = []

        with open(self.path_labels_csv, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter=',', dialect='excel')
            rows = list(reader)
            for row in rows[1:]:
                # train
                # id, url, landmark_id
                # index, test
                # id, url
                path = os.path.join(self.config['data_loader']['data_dir'], '%s-resized256' % self.name,
                                    '%s.jpg' % row[0])
                if '2018' in self.config['data_loader']['data_dir']:
                    url = row[1]
                else:
                    url = 'dummy_url'

                if os.path.exists(path) and str(url) != 'None':  # if image is downloaded
                    if self.name == 'train':
                        label = int(row[2])
                        labels.append(label)  # i want to enumerate classes starting from 0
                    else:
                        labels.append(-42)
                    images.append(path)

        images = np.array(images)
        labels = np.array(labels)
        self.classes_number = 14951  # np.unique(labels).shape[0]
        print('np.unique(labels) ', np.sort(np.unique(labels)))
        print('images.shape ', images.shape)
        return images, labels
