import torch
from results.metric import get_nearest_neighbors
import numpy as np


# Ondrej Chum, James Philbin, Josef Sivic, Michael Isard, Andrew Zisserman
# \textit{Total Recall: Automatic Query Expansion with a Generative Feature Model for Object Retrieval}
# 2007

# Ondrej Chum, Andrej Mikulik, Michal Perdoch, Jiri Matas.
# \textit{Total Recall II: Query Expansion Revisited}.
# 2011

# this is the very basic query expansion
# we just append
# the result for the average of query and it's k nearest neighbors
# to our initial result
def query_expansion(index_set, i, s, k):
    new_queries = torch.mean(index_set[i], dim=1)  # we average query and it's k nearest neighbors
    s_new, i_new = get_nearest_neighbors(new_queries, index_set, k)
    i_new = np.hstack((i, i_new))
    s_new = np.hstack((s, s_new))
    return i_new, s_new
