import faiss
import numpy as np
import torch


# queries = np.array([[1.0, 2.0],
#            [3.0, 4.0],
#            [5.0, 6.0]])
# queries_norm = np.array([np.linalg.norm(queries, axis=1) , np.linalg.norm(queries, axis=1)]).transpose()
# print('queries_norm ', queries_norm)
# queries = queries/queries_norm
# queries = torch.from_numpy(queries)
#
# index_set = np.array([[1.0, 2.0],
#            [3.0, 4.0],
#            [5.0, 6.0],
#              [5.0, 6.0],
# [7.0, 8.0],
# [9.0, 10.0],
# [11.0, 12.0],
# [13.0, 14.0],
# [15.0, 16.0],
# [17.0, 18.0],
# [19.0, 20.0],
# [21.0, 22.0],
#              [23.0, 24.0],
# [25.0, 26.0],
# [27.0, 28.0],
# [29.0, 30.0],
#             ])
#
# index_set_norm = np.array([np.linalg.norm(index_set, axis=1) , np.linalg.norm(index_set, axis=1)]).transpose()
# print('index_set_norm ', index_set_norm)
# index_set = index_set/index_set_norm
# index_set = torch.from_numpy(index_set)
#
# print('queries', queries)
# print('index_set ', index_set)

def index_set_aug(queries, index_set):
    dimension = queries.shape[1]
    queries = queries.cpu().numpy().astype('float32')
    index_set = index_set.cpu().numpy().astype('float32')

    new_index_set, resource = get_new_index_set(dimension, index_set, queries)

    index = faiss.GpuIndexFlatIP(resource, dimension)
    index.add(new_index_set)

    neighbors_similarities, neighbors_indices = index.search(queries, k=100)
    return neighbors_similarities, neighbors_indices


def get_new_index_set(dimension, index_set, queries):
    # resources = [faiss.StandardGpuResources() for j in range(faiss.get_num_gpus())]
    # resource = resources[1]
    resource = faiss.StandardGpuResources()
    queries_number = queries.shape[0]
    # index_to_test = np.random.randint(low=0, high=queries_number)
    # assert np.abs(np.linalg.norm(queries[index_to_test]) - 1.0) <= 10e-1, \
    #     'Cosine similarity nearest neighbors search should work with normalized data! ' \
    #     'But np.linalg.norm(queries[%d] = %.10e' % (index_to_test, np.linalg.norm(queries[index_to_test]))
    index = faiss.GpuIndexFlatIP(resource, dimension)
    index.add(index_set)
    _, index_10_neighbors_indices = index.search(index_set, k=10)
    # print('index_10_neighbors_indices ', index_10_neighbors_indices)
    # print('index_set[index_10_neighbors_indices] ', index_set[index_10_neighbors_indices],
    #      index_set[index_10_neighbors_indices].shape)
    new_index_set = index_set[index_10_neighbors_indices].mean(axis=1)
    print('new_index_set ', new_index_set, new_index_set.shape)
    return new_index_set, resource

# index_set_aug(queries, index_set)
