# Yanzhi Chen, Xi Lin, Anthony Dick, Rhys Hill.
# \textit{Ranking consistency for image matching and object retrieval}.
# 2013.

import numpy as np
from tqdm import tqdm

from improvement.query_expansion import query_expansion
from improvement.rbo import rbo_at_k
from results.metric import get_nearest_neighbors


def calculate_rbo_at_k(x, y, p=0.9, k=100):
    return rbo_at_k(x.tolist(), y.tolist(), p, depth=k)


def the_most_similar_entry(answers, answers_list, k):
    arr = [calculate_rbo_at_k(answers, x, p=0.9, k=k) for x in answers_list]
    # print('answers_list', len(answers_list), 'arr ', len(arr))
    return max(enumerate(arr), key=lambda x: x[1])[0]


def rerank(query_result, index_set, k):
    neighbors = index_set[query_result, :]
    _, answers_for_neighbors = get_nearest_neighbors(neighbors, index_set, k + 1)
    answers = []
    latest_item = query_result
    for i in range(k):
        # print('i = ', i, 'latest_item', len(latest_item), 'answers_for_neighbors ', len(answers_for_neighbors))
        highest_ind = the_most_similar_entry(latest_item, answers_for_neighbors, k=k)
        latest_item = answers_for_neighbors[highest_ind, :]
        answers.append(query_result[highest_ind])
        answers_for_neighbors = np.delete(answers_for_neighbors, highest_ind, axis=0)
        query_result = np.delete(query_result, highest_ind, axis=0)
    return answers


def reranking(index_set, i, s, k):
    queries_number = i.shape[0]
    print('Reranking is in progress...')
    print('queries_number ', queries_number)
    for j in tqdm(range(queries_number)):
        i[j, :] = rerank(i[j, :], index_set, k)
        s[j, :] = s[j, [np.where(i[j] == a)[0][0] for a in i[j]]]
    return i, s


def expansion_and_reranking(i, s, index_set, times, k=1):
    i_new = i[:, :k + 1]
    s_new = s[:, :k + 1]
    for j in range(1, times + 1):
        i_new, s_new = query_expansion(index_set, i_new, s_new, k=k)
        print('query_expansion is ok (j + 1) * (k + 1)', (j + 1) * k)
        print('i_new ', i_new.shape, ' s_new ', s_new.shape)
        i_new, s_new = reranking(index_set, i_new, s_new, k=(j + 1) * k)
    return i_new, s_new
