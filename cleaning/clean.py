import csv
import os
import sys

import numpy as np
import pandas as pd
from PIL import Image
from tqdm import tqdm

module_path = '/data/landmarks-2019'  # os.path.abspath(os.path.join('..'))
print('module_path ', module_path)

if module_path not in sys.path:
    sys.path.insert(0, module_path)  # sys.path.append(module_path)

data_path = os.path.join(module_path, 'cleaning')
if data_path not in sys.path:
    sys.path.insert(0, data_path)

from vgg16_places_365 import VGG16_Places365

image_samples = '/data/landmarks2019-data/2019/test-resized256/'
all_images = os.listdir(image_samples)

all_images_resized = []
for filename in tqdm(all_images):
    im = np.array(Image.open(image_samples + filename).resize((224, 224), Image.LANCZOS))
    all_images_resized.append(im)

# Placeholders for predictions
p0, p1, p2 = [], [], []

# Places365 Model
model = VGG16_Places365(weights='places')
topn = 5

# Loop through all images
for image in tqdm(all_images_resized):
    # Predict Top N Image Classes
    image = np.expand_dims(image, 0)
    prediction = model.predict(image)
    topn_preds = np.argsort(prediction[0])[::-1][0:topn]

    p0.append(topn_preds[0])
    p1.append(topn_preds[1])
    p2.append(topn_preds[2])

# Create dataframe for later usage
topn_df = pd.DataFrame()
topn_df['filename'] = np.array(all_images)
topn_df['p0'] = np.array(p0)
topn_df['p1'] = np.array(p1)
topn_df['p2'] = np.array(p2)
topn_df.to_csv('topn_class_numbers.csv', index=False)

# Summary
print(topn_df.head())

# Read Class number, class name and class indoor/outdoor marker
class_information = pd.read_csv('extended.csv')
print(class_information.head())

# Set Class Labels
for col in ['p0', 'p1', 'p2']:
    # topn_df[col + '_label'] = topn_df[col].map(class_information.set_index('class')['label'])
    topn_df[col + '_landmark'] = topn_df[col].map(
        class_information.set_index('class')['io'].replace({1: 'non-landmark', 2: 'landmark'}))

topn_df.drop('p0', axis=1, inplace=True)
topn_df.drop('p1', axis=1, inplace=True)
topn_df.drop('p2', axis=1, inplace=True)
topn_df.to_csv('topn_all_info.csv', index=False)

test_clean_ids = []
with open('topn_all_info.csv', 'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', dialect='excel')
    rows = list(reader)
    for row in tqdm(rows[1:]):
        test_id = row[0]
        landmarks = [row[1], row[2], row[3]]
        if landmarks.count('landmark') >= 2:
            test_clean_ids.append(test_id)

with open('test_clean.csv', "w") as fout:
    fout.write('id\n')
    for i, key in tqdm(enumerate(test_clean_ids)):
        fout.write(key[:-4])
        fout.write('\n')

# Summary
print(topn_df.head())

# Get 'landmark' images
n = 9
landmark_images = topn_df[topn_df['p0_landmark'] == 'landmark']['filename']
landmark_indexes = landmark_images[:n].index.values
