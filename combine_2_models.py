import jstyleson
import torch

from improvement.db_aug import index_set_aug
from model.modules.l2_normalization import L2Normalization
from utils.util import get_loaders, load_inference
import numpy as np


def get_test_and_index_with_name(name, loaders_dict):
    test_loader = loaders_dict['test_loader']
    index_loader = loaders_dict['index_loader']

    test_loader.dataset.name = 'test-%s' % name
    index_loader.dataset.name = 'index-%s' % name

    result_dict_index = load_inference(index_loader, batches_number_real=False)
    result_dict_test = load_inference(test_loader, batches_number_real=False)

    all_outputs_index = result_dict_index['all_outputs']
    all_outputs_test = result_dict_test['all_outputs']

    return all_outputs_test, all_outputs_index


config = jstyleson.load(open('config.json'))
loaders_dict = get_loaders(config)

all_outputs_test_gem, all_outputs_index_gem = get_test_and_index_with_name('gem', loaders_dict)
all_outputs_test_rmac, all_outputs_index_rmac = get_test_and_index_with_name('rmac', loaders_dict)
print('all_outputs_index gem', all_outputs_index_gem.shape)
print('all_outputs_index rmac', all_outputs_index_rmac.shape)

all_outputs_index = torch.cat((all_outputs_index_gem, all_outputs_index_rmac), dim=1)
all_outputs_test = torch.cat((all_outputs_test_gem, all_outputs_test_rmac), dim=1)

all_outputs_index = L2Normalization()(all_outputs_index)
all_outputs_test = L2Normalization()(all_outputs_test)

print('all_outputs_index ', all_outputs_index.shape)
print('all_outputs_test ', all_outputs_test.shape)

neighbors_similarities, neighbors_indices = index_set_aug(queries=all_outputs_test,
                                                          index_set=all_outputs_index)

np.save('100_nearest_neighbors_combined', neighbors_indices)
np.save('100_nearest_neighbors_similarities_combined', neighbors_similarities)
