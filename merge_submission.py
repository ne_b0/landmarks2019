import csv
import numpy as np
from gevent import os
from tqdm import tqdm

test_ids = []
first_neighbors = []
with open(
        'natasha_submission_rmac-0.319lb-checkpoint-epoch001-loss-0.0261.pth.tar-cosi+ring-loss-layer1unfreeze-resnet50-with_db-aug-10.csv',
        'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', dialect='excel')
    rows = list(reader)
    for row in tqdm(rows[1:]):
        test_id = row[0]
        neighbors_ids = row[1].split(' ')
        test_ids.append(test_id)
        first_neighbors.append(neighbors_ids)

second_neighbors = []
with open(
        'natasha_submission_gem_checkpoint-epoch001-loss-0.0345.pth.tar-cosi+ring-loss-layer1unfreeze-resnet50-with_db-aug-10.csv',
        'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', dialect='excel')
    rows = list(reader)
    for row in tqdm(rows[1:]):
        test_id = row[0]
        neighbors_ids = row[1].split(' ')
        second_neighbors.append(neighbors_ids)

num_of_neighbors = np.zeros(len(test_ids))
intersected_neighbors = []
for i, test in enumerate(test_ids):
    neighbors = []
    for j, neighbor in enumerate(first_neighbors[i]):
        if neighbor in second_neighbors[i] or j < 50:
            neighbors.append(neighbor)
    intersected_neighbors.append(neighbors)
    num_of_neighbors[i] = len(neighbors)

print('num_of_neighbors ', num_of_neighbors.mean(), num_of_neighbors)

with open('natasha_submission_merged.csv', "w") as fout:
    fout.write('id,images\n')
    for i, key in tqdm(enumerate(test_ids)):
        fout.write(key + ',')
        fout.write(" ".join([str(el) for el in intersected_neighbors[i]]))
        fout.write('\n')
