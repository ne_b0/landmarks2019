import csv
import os

import jstyleson
import numpy as np
import pretrainedmodels
import torch
from torch import nn
import download_data
from data_loader.data_loaders import Landmark2019DataLoader
from model.model import Landmark2019Model
from results.knn import nearest_neighbors
from utils.util import get_loaders, freeze_layers
from tqdm import tqdm


def get_all_keys(filename):
    keys_urls = download_data.ParseData(filename)
    all_keys = []
    for i, pair in enumerate(keys_urls):
        all_keys.append(pair[0])
    return all_keys


def get_existing_keys(filename):
    with open(filename, "r") as fin:
        line = fin.readline()
    existing_keys = np.array(list(map(str, line.split())))
    for i, key in enumerate(existing_keys):
        existing_keys[i] = existing_keys[i].replace('.jpg', '')
    return existing_keys


def save_nearest_neighbors_to_file():
    global pretrained_net
    config = jstyleson.load(open('config.json'))
    loaders_dict = get_loaders(config)
    test_loader = loaders_dict['test_loader']
    index_loader = loaders_dict['index_loader']

    if config['model_type'] == 'radenovich':
        model = Landmark2019Model(config=config, loader_for_pca_initialization=None,
                                  basic_net=None)
        # print('Radenovich checkpoint loading')
        # check_point_filename = 'retrievalSfM120k-resnet101-gem-b80fb85.pth'
        # checkpoint_for_model = torch.load(
        #     '/data/landmarks2019-data/%s' % check_point_filename)
        # model.net.load_state_dict(checkpoint_for_model['state_dict'])
        # print('Checkpoint %s is loaded!' % check_point_filename)
        # input()
    else:
        model = create_a_model_of_natasha_type(config, loaders_dict)

        check_point_filename = 'rmac-0.319lb-checkpoint-epoch001-loss-0.0261.pth.tar'
        checkpoint_for_model = torch.load(
            '/data/landmarks2019/saved/Landmark2019/representation/histogram_loss/Landmark2019/'
            '%s' % check_point_filename)
        model.load_state_dict(checkpoint_for_model['state_dict'])
        print('Checkpoint %s is loaded!' % check_point_filename)

    nearest_neighbors(model=model, loader_index=index_loader, loader_test=test_loader, k=100,
                      inference='do')


def create_a_model_of_natasha_type(config, loaders_dict):
    global pretrained_net
    train_loader_for_evaluation = loaders_dict['train_loader_for_evaluation']
    train_loader_for_classification = Landmark2019DataLoader(config, name='train', for_classification=True)

    pretrained_net = pretrainedmodels.se_resnext101_32x4d()  # pretrainedmodels.se_resnext50_32x4d()  #
    print('pretrained_net', pretrained_net)
    train_classes_number = train_loader_for_classification.dataset.classes_number
    print('train_classes_number ', train_classes_number)

    # for se_resnext50_32x4d
    pretrained_net.last_linear = nn.Linear(pretrained_net.last_linear.in_features, train_classes_number)
    checkpoint_for_resnet = torch.load(config['pretrained_resnet_file'])
    # pretrained_net.load_state_dict(checkpoint_for_resnet['state_dict'])

    model = Landmark2019Model(config=config, loader_for_pca_initialization=train_loader_for_evaluation,
                              basic_net=pretrained_net)
    pretrained_net = None
    return model


def get_neighbors(neighbors, existing_index_keys, existing_test_keys, test_key, similarities):
    mean = similarities.mean()
    index = np.where(existing_test_keys.__eq__(str(test_key)))[0][0]
    # print(test_key, ' index ', index)
    # print('np.where(similarities[index] >= mean)[0] ', np.where(similarities[index] >= mean)[0])
    # good_similarities = np.where(similarities[index] >= mean)[0]
    neighbors_indices = np.asarray(neighbors[index], dtype=int)
    # print('neighbors_indices ', neighbors_indices)
    return existing_index_keys[neighbors_indices]


def get_dummy_neighbors(existing_index_keys):
    result = []
    for i in np.random.random_integers(low=0, high=existing_index_keys.shape[0] - 1, size=100):
        result.append(existing_index_keys[i])
    return result


print('start')
existing_keys = []
with open('/data/landmarks2019-data/2019/test.csv', 'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', dialect='excel')
    rows = list(reader)
    for row in tqdm(rows[1:]):
        # train
        # id, url, landmark_id
        # index, test
        # id, url
        path = os.path.join('/data/landmarks2019-data/2019/test-resized256/%s.jpg' % row[0])
        # url = row[1]
        key = row[0]
        if os.path.exists(path):  #and str(url) != 'None':  # if image is downloaded
            existing_keys.append(key)
existing_keys = np.array(existing_keys)

existing_index_keys = []
with open('/data/landmarks2019-data/2019/index.csv', 'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', dialect='excel')
    rows = list(reader)
    for row in tqdm(rows[1:]):
        # train
        # id, url, landmark_id
        # index, test
        # id, url
        path = os.path.join('/data/landmarks2019-data/2019/index-resized256/%s.jpg' % row[0])
        # url = row[1]
        key = row[0]
        if os.path.exists(path):  # and str(url) != 'None':  # if image is downloaded
            existing_index_keys.append(key)
existing_index_keys = np.array(existing_index_keys)

all_test_keys = existing_keys  #get_all_keys('/data/landmarks2019/retrieval_sample_submission.csv')

print('all_test_keys ', len(all_test_keys))
print('existing_keys ', len(existing_keys))
print('existing_index_keys ', len(existing_index_keys))

save_nearest_neighbors_to_file()

neighbors = np.load('100_nearest_neighbors.npy')
similarities = np.load('100_nearest_neighbors_similarities.npy')

print('first 30 similarities', similarities[:, :30])
print('mean similarity', similarities.mean())
print('neighbors', neighbors.shape)

# id,images
# 000088da12d664db,0370c4c856f096e8 766677ab964f4311 e3ae4dcee8133159...
# etc.
count_dummy = 0
print('existing_keys[0] ', existing_keys[0])
with open('natasha_submission_radenovich-with_db-aug-10.csv',
          "w") as fout:
    fout.write('id,images\n')
    for key in tqdm(all_test_keys):
        fout.write(key + ',')
        if key in existing_keys:
            neighbors_list = get_neighbors(neighbors, existing_index_keys, existing_keys, key, similarities)
        else:
            neighbors_list = []  # get_dummy_neighbors(existing_index_keys)
            count_dummy = count_dummy + 1
            print('count_dummy', count_dummy)
        fout.write(" ".join([str(el) for el in neighbors_list]))
        fout.write('\n')

print('count_dummy', count_dummy)
