import datetime
import os
import numpy as np
import torch
from base.base_trainer import BaseTrainer
# from results.metric import *
from utils.plot_utils import plot_loss


class ClassificationTrainer(BaseTrainer):
    """
    Classification Trainer class

    Note:
        Inherited from BaseTrainer.
        self.optimizer is by default handled by BaseTrainer based on config.
    """

    def __init__(self, model, loss, metrics, resume, config,
                 train_loader, test_loader=None, train_logger=None):
        super(ClassificationTrainer, self).__init__(model, loss, metrics, resume, config, train_loader, test_loader,
                                                    train_logger)

        self.loss_name = self.config['classification_loss']
        self.monitor = config['pre_classification_trainer']['monitor']
        self.monitor_mode = config['pre_classification_trainer']['monitor_mode']
        self.epochs = config['pre_classification_trainer']['epochs']
        self.checkpoint_dir = os.path.join(config['pre_classification_trainer']['save_dir'], self.name)

        print('ClassificationTrainer self.model ', self.model)

    def _eval_metrics(self, output, target):
        # print('Start _eval_metrics')
        accumulated_metrics = np.zeros(len(self.metrics))
        # print('accumulated_metrics ', accumulated_metrics)
        output = output.cpu().data.numpy()
        target = target.cpu().data.numpy()
        output = np.argmax(output, axis=1)
        for i, metric in enumerate(self.metrics):
            accumulated_metrics[i] += metric(output, target)
        return accumulated_metrics

    def _train_epoch(self, epoch):
        """
        Training logic for an epoch

        :param epoch: Current training epoch.
        :return: A log that contains all information you want to save.

        Note:
            If you have additional information to record, for example:
                > additional_log = {"x": x, "y": y}
            merge it with log before return. i.e.
                > log = {**log, **additional_log}
                > return log

            The metrics in log must have the key 'metrics'.
        """
        self.model.train()

        total_loss = 0
        total_metrics = np.zeros(len(self.metrics))
        print('self.train_loader ', self.train_loader.batch_size, self.train_loader)

        for batch_idx, (data, target, index) in enumerate(self.train_loader):
            data, target = self._to_tensor(data, target)

            self.optimizer.zero_grad()
            output = self.model(data)
            loss = self.loss(output, target, self.config)
            loss.backward()
            self.optimizer.step()

            # print('loss ', loss)
            # print('loss.item() ', loss.item())
            # print('target ', target)
            total_loss += loss.item()
            total_metrics += self._eval_metrics(output, target)

            if self.verbosity >= 2 and batch_idx % self.log_step == 0:
                self.logger.info('Train Epoch: {} [{}/{} ({:.0f}%)] Loss: {:.6f} time {}'.format(
                    epoch,
                    batch_idx * self.train_loader.batch_size,
                    len(self.train_loader) * self.train_loader.batch_size,
                    100.0 * batch_idx / len(self.train_loader),
                    loss.item(),
                    datetime.datetime.now()))

            # plot_loss(self, batch_idx, 0, epoch, loss)

        log = {
            'loss': total_loss / len(self.train_loader),
            'metrics': (total_metrics / len(self.train_loader)).tolist()
        }

        if self.valid and epoch == self.config['pre_classification_trainer']['epochs']:
            val_log = self._valid_epoch()
            log = {**log, **val_log}

        return log

    def _valid_epoch(self):
        """
        Validate after training an epoch

        :return: A log that contains information about validation

        Note:
            The validation metrics in log must have the key 'val_metrics'.
        """
        print('Start _valid_epoch')
        self.model.eval()
        total_val_loss = 0
        total_val_metrics = np.zeros(len(self.metrics))
        with torch.no_grad():
            for batch_idx, (data, target, indices) in enumerate(self.test_loader):
                data, target = self._to_tensor(data, target)

                output = self.model(data)
                loss = self.loss(output, target, self.config)

                total_val_loss += loss.item()
                total_val_metrics += self._eval_metrics(output, target)

        return {
            'val_loss': total_val_loss / len(self.test_loader),
            'val_metrics': (total_val_metrics / len(self.test_loader)).tolist()
        }
