import datetime

import numpy as np
import torch
import visdom

from base.base_trainer import BaseTrainer
from results.evaluation_utils import evaluate_map_on_dataset
from utils.plot_utils import plot_loss


class RepresentationTrainer(BaseTrainer):
    """
    Trainer class

    Note:
        Inherited from BaseTrainer.
        self.optimizer is by default handled by BaseTrainer based on config.
    """

    def __init__(self, model, loss, metrics, resume, config, train_loader, test_loader=None, train_logger=None):
        super(RepresentationTrainer, self).__init__(model, loss, metrics, resume, config,
                                                    train_loader=train_loader, test_loader=test_loader,
                                                    train_logger=train_logger)

        self.loss_name = self.config['loss']['name']

    def _train_epoch(self, epoch):
        """
        Training logic for an epoch

        :param epoch: Current training epoch.
        :return: A log that contains all information you want to save.

        Note:
            If you have additional information to record, for example:
                > additional_log = {"x": x, "y": y}
            merge it with log before return. i.e.
                > log = {**log, **additional_log}
                > return log

        """
        self.model.train()

        total_loss = 0
        print('epoch ', epoch, ' self.config[\'sampling\'][\'stage\'] ', self.config['sampling']['stage'])
        print('self.train_loader ', self.train_loader, self.train_loader.batch_size, self.train_loader.dataset)
        print('self.optimizer ', self.optimizer)

        # This is default batch to batch processing in case our sampling strategy is on the batch level
        for batch_idx, (data, target, indices) in enumerate(self.train_loader):
            # print('batch_idx ', batch_idx)
            data, target = self._to_tensor(data, target)

            if self.config['2head']:
                output, for_softmax = self.model(data)
            else:
                output = self.model(data)
                for_softmax = None

            total_loss = self.optimize_print_plot(batch_idx, 0,
                                                  epoch,
                                                  output, output, target, target,
                                                  for_softmax,
                                                  total_loss)

        # print('self.model.spoc.spoc_layer.p ', self.model.spoc.spoc_layer.p, end=' ')
        log = {
            'loss': total_loss / len(self.train_loader)
        }

        # if self.valid:
        #     val_log = self._valid_epoch(epoch)
        #     log = {**log, **val_log}

        with torch.cuda.device(self.config['gpu']):
            torch.cuda.empty_cache()
        return log

    def optimize_print_plot(self, batch_idx_1, batch_idx_2,
                            epoch,
                            output_1, output_2,
                            target_1, target_2,
                            for_softmax,
                            total_loss):
        self.optimizer.zero_grad()

        if self.config['2head']:
            softmax_loss = self.loss(for_softmax, target_1, self.config)
            center_loss = self.center_loss(output_1.cuda())
            print('sl ', softmax_loss, ' cl ', center_loss)
            loss = softmax_loss + center_loss
            loss.backward()

        else:
            loss = self.loss(output_1, output_2, target_1, target_2, self.config)
            loss.backward()



        self.optimizer.step()
        total_loss += loss.item()
        self.print_log(batch_idx_1, batch_idx_2, epoch, loss)
        # plot_loss(self, batch_idx_1, batch_idx_2, epoch, loss)
        return total_loss

    def print_log(self, batch_idx_1, batch_idx_2, epoch, loss):
        if self.verbosity >= 2 and (batch_idx_1 % self.log_step == 0 and batch_idx_2 == 0):
            self.logger.info('Train Epoch: {} [{}/{} ({:.0f}%)] Loss: {:.16f} time {}'.format(
                epoch,
                batch_idx_1 * self.train_loader.batch_size + batch_idx_2,
                len(self.train_loader) * self.train_loader.batch_size,
                100.0 * batch_idx_1 / len(self.train_loader),
                loss.item(),
                datetime.datetime.now()))

    def _valid_epoch(self, epoch):
        """
        Validate after training an epoch

        """
        torch.cuda.empty_cache()
        self.model.eval()
        print('*' * 100)
