#!/usr/bin/python

# Note to Kagglers: This script will not run directly in Kaggle kernels. You
# need to download it and run it on your local machine.

# Downloads images from the Google Landmarks dataset using multiple threads.
# Images that already exist will not be downloaded again, so the script can
# resume a partially completed download. All images will be saved in the JPG
# format with 90% compression quality.

import sys, os, multiprocessing, csv
from PIL import Image

data_file = '/data/landmarks2019-data/2019/index.csv'
out_dir = '/data/landmarks2019-data/2019/index-resized256/'


def ParseData(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_list = [line[:1] for line in csvreader]
    return key_list[1:]  # Chop off header


def ParseDataWithLabels(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_label_list = [line[:3] for line in csvreader]
    return key_label_list[1:]  # Chop off header


def DownloadImage(key):
    key = key[0]
    filename = os.path.join(out_dir, '%s.jpg' % key)

    if os.path.exists(filename):
        # print('Image %s already exists. Skipping download.' % filename)
        return
    #
    # print('key ', key)
    #
    # print('image to open /data/landmarks2019-data/2019/test/%s/%s/%s/%s.jpg' % (key[0][0], key[0][1], key[0][2], key[0]))
    # input()

    try:
        # natasha do not forget to change here

        pil_image = Image.open('/data/landmarks2019-data/2019/index/%s/%s/%s/%s.jpg' % (key[0], key[1], key[2], key))
        # print(pil_image)
    except Exception:
        print('Warning: Failed to parse image %s' % key)
        return

    try:
        pil_image_resized = pil_image.resize((256, 256))
        pil_image_resized.save(filename, format='JPEG', quality=90)
    except Exception:
        print('Warning: Failed to save image %s' % filename)
        return


def Run():
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    key_list = ParseData(data_file)
    pool = multiprocessing.Pool(processes=8)
    pool.map(DownloadImage, key_list)


if __name__ == '__main__':
    Run()
