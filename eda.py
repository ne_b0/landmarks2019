import numpy as np
import pandas as pd

# similarities = np.load('100_nearest_neighbors_similarities.npy')
#
# print('first 30 similarities', similarities[:, :30])
# print('last 30 similarities', similarities[:, :-30])
# print('mean similarity', similarities.mean())
# mean = similarities.mean()
# print(np.where(similarities > mean)[0].shape[0]/(similarities.shape[0]*similarities.shape[1]))

dataframe = pd.read_csv('natasha_submission_spoc_checkpoint-epoch001-loss-0.0304-with_db-aug-short.csv')
ids = []
images = []
for id, image in zip(np.array(dataframe['id']), np.array(dataframe['images'])):
    ids.append(id)

    image = str(image)[:50 * 17]
    images.append(image)

with open('natasha_submission_spoc_checkpoint-epoch001-loss-0.0304-with_db-aug-10-half.csv', "w") as fout:
    fout.write('id,images\n')
    for id, image in zip(ids, images):
        if str(image) != 'nan':
            fout.write(id + ',' + image + '\n')
        else:
            fout.write(id + ',' + '' + '\n')
