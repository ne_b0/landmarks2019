import datetime
import os

import torch

import numpy as np
from sklearn.manifold import TSNE
from visdom import Visdom

from utils.util import read_inference_results_from_disk, load_inference


def plot_loss(base_trainer, batch_idx_1, batch_idx_2, epoch, loss):
    base_trainer.r_loss.append(loss.item())

    if base_trainer.config['sampling']['stage'] == 'loss':
        base_trainer.iterations.append(epoch * len(base_trainer.train_loader) * len(base_trainer.train_loader) +
                                       batch_idx_1 * len(base_trainer.train_loader) + batch_idx_2)
    else:
        base_trainer.iterations.append(epoch * len(base_trainer.train_loader) + batch_idx_1)

    base_trainer.loss_plot = base_trainer.vis.line(
        Y=np.array(base_trainer.r_loss),
        X=np.array(base_trainer.iterations),
        win=base_trainer.loss_plot,
        opts=dict(
            showlegend=True,
            xlabel='Iterations',
            ylabel='Loss',
            title=base_trainer.loss_name,
            marginleft=30,
            marginright=30,
            marginbottom=80,
            margintop=30,
        ),
        name='loss for train'
    )


def plot_map(representation_trainer, epoch, map_for_test, map_for_train):
    representation_trainer.epochs_for_plot.append(epoch)
    representation_trainer.map_for_train.append(map_for_train)
    representation_trainer.map_for_test.append(map_for_test)
    representation_trainer.map_plot = representation_trainer.vis.line(
        Y=np.column_stack((np.array(representation_trainer.map_for_train),
                           np.array(representation_trainer.map_for_test))),
        X=np.column_stack((np.array(representation_trainer.epochs_for_plot),
                           np.array(representation_trainer.epochs_for_plot))),
        win=representation_trainer.map_plot,
        opts=dict(
            showlegend=True,
            xlabel='epochs',
            ylabel='MAP at %d' % representation_trainer.config['metrics']['k_for_MAP'],
            title='MAP plot for %s' % representation_trainer.config['loss']['name'],
            marginleft=30,
            marginright=30,
            marginbottom=80,
            margintop=30,
            legend=['MAP for train', 'MAP for test']
        ),
        name='MAP'
    )


def plot_images(representation_trainer, loader, win, query_loader=None):
    config = loader.config
    if representation_trainer is not None:
        vis = representation_trainer.images_vis
        title = 'Retrieval results for %s, epoch = %d' % (
            loader.dataset.name, representation_trainer.epochs_for_plot[-1])
    else:
        vis = Visdom()
        title = loader.dataset.path_labels_csv

    nearest_neighbors_filename = 'nearest_neighbors_%s' % loader.dataset.name
    if nearest_neighbors_filename not in os.listdir(config['temp_folder']):
        print('Visualization is not possible, check your config!')
    else:
        queries_number = config['visualization']['queries_number_for_visualization']
        k = config['metrics']['k_for_MAP']
        dataset = loader.dataset
        if query_loader is None:
            queries_dataset = loader.dataset
        else:
            queries_dataset = query_loader.dataset

        nearest_neighbors = torch.load(os.path.join(config['temp_folder'], nearest_neighbors_filename))
        result_dict = load_inference(loader)
        all_indices = result_dict['all_indices']

        result_dict = load_inference(query_loader)
        all_indices_queries = result_dict['all_indices']

        images_block = torch.FloatTensor()

        for query_number in range(queries_number):
            query, label = queries_dataset.get_image_and_label_for_visualization(all_indices_queries[query_number])
            query = query.unsqueeze(dim=0)

            answers = torch.FloatTensor()
            for neighbor_number in range(k):
                index_among_the_nearest_neighbors = nearest_neighbors[query_number, neighbor_number]
                answer, label = \
                    dataset.get_image_and_label_for_visualization(all_indices[index_among_the_nearest_neighbors])

                answer = answer.unsqueeze(dim=0)
                answers = torch.cat((answers, answer), dim=0)

            row = torch.cat((query, answers), dim=0)

            images_block = torch.cat((images_block, row), dim=0)

        vis.images(images_block, nrow=k + 1, win=win, opts=dict(
            caption='The very left column consists of query images. \n'
                    'Each row consists of top %d retrieval results for the corresponding query' % k,
            title=title
        ))


def generate_colors(colors_number=8):
    np.random.seed(seed=86)
    r = np.random.randint(low=0, high=256, size=(colors_number, 1))
    g = np.random.randint(low=0, high=256, size=(colors_number, 1))
    b = np.random.randint(low=0, high=256, size=(colors_number, 1))
    colors = np.hstack((r, g, b))
    return colors


def plot_tsne(representation_trainer, loader, win):
    print('Start tsne visualization ', datetime.datetime.now())
    embeddings_tsne, labels = create_embeddings_for_tsne(loader)
    labels = [str(l) for l in list(labels.cpu().data.numpy())]
    unique_labels = np.unique(np.array(labels))
    colors = generate_colors(colors_number=len(unique_labels))
    marker_colors = np.array([colors[np.where(unique_labels == l)[0][0]] for l in labels], dtype=int)
    if representation_trainer is not None:
        vis = representation_trainer.tsne_vis
        title = '%s epoch = %d' % (loader.dataset.name, representation_trainer.epochs_for_plot[-1])
        name = title
    else:
        vis = Visdom()
        title = loader.dataset.path_labels_csv
        name = title

    vis.scatter(
        torch.from_numpy(embeddings_tsne),
        win=win,
        opts=dict(
            textlabels=labels,
            markersize=10,
            markercolor=marker_colors,
            title=title
        ),
        name=name
    )


def create_embeddings_for_tsne(loader):
    config = loader.config
    pack_volume = config['pack_volume']
    path = os.path.join(config['temp_folder'], loader.dataset.name, '')
    print('path in create embeddings ', path)
    batches_number = len(loader) // pack_volume
    name = loader.dataset.name
    result_dict = read_inference_results_from_disk(config, batches_number, name)
    labels, embeddings = result_dict['all_labels'], result_dict['all_outputs']
    embeddings = embeddings.cpu().data.numpy()
    embeddings_tsne = TSNE(n_components=2).fit_transform(embeddings)
    return embeddings_tsne, labels
