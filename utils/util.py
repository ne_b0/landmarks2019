import datetime
import os

import numpy as np
import torch
from sklearn.metrics.pairwise import cosine_similarity
from torch.autograd import Variable
from tqdm import tqdm

from data_loader.data_loaders import Landmark2019DataLoader


def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def outputs_for_large_dataset(loader, network):
    """

    :param loader: should be of type Landmark2019DataLoader
    :param network: should be an inheritor of torch.nn.Module
    :return: the inference result for all samples of the dataset loaded by loader and
    labels for these samples as torch Tensors
    """
    config = loader.config
    with torch.cuda.device(config['gpu']):
        torch.cuda.empty_cache()
        name = loader.dataset.name
        batches_number = save_inference_results_on_disk(loader, network, name)
        result_dict = read_inference_results_from_disk(config, batches_number, name)
        return result_dict


def read_inference_results_from_disk(config, batches_number, name):
    path = os.path.join(config['temp_folder'], name, '')
    pack_volume = config['pack_volume']
    assert 'all_outputs_%d' % pack_volume in os.listdir(path), \
        'There should be precomputed inference data in %s!' % path

    # This is GPU version if we have enough GPU memory

    with torch.cuda.device(config['gpu']):
        all_outputs = torch.cuda.FloatTensor()

        print('batches_number + 1 in read', batches_number + 1)
        for i in tqdm(range(1, batches_number + 1)):
            outputs = torch.load('%sall_outputs_%d' % (path, i * pack_volume))
            all_outputs = torch.cat((all_outputs, outputs), dim=0)

    all_outputs = all_outputs.detach().cpu()


    # This is CPU version for descriptors of 2048 size

    # outputs_for_dim = torch.load('%sall_outputs_%d' % (path, pack_volume))
    # last_outputs_for_dim = torch.load('%sall_outputs_%d' % (path, batches_number * pack_volume))
    #
    # batch_size = outputs_for_dim.shape[1]
    # descriptor_dimension = last_outputs_for_dim.shape[0]
    # last_batch_size = last_outputs_for_dim.shape[1]
    #
    # all_outputs = torch.zeros((batches_number - 1) * batch_size + last_batch_size, descriptor_dimension)
    # print('all_outputs.shape for zeros ', all_outputs.shape)
    # all_indices = torch.LongTensor()
    # print('batches_number + 1 in read', batches_number + 1)
    # for i in tqdm(range(1, batches_number + 1)):
    #     outputs = torch.load('%sall_outputs_%d' % (path, i * pack_volume)).cpu()
    #     indices = torch.load('%sall_indices_%d' % (path, i * pack_volume)).cpu()
    #
    #     outputs = torch.transpose(outputs, dim0=1, dim1=0)
    #     all_outputs[(i - 1) * pack_volume * batch_size: i * pack_volume * batch_size] = outputs
    #     all_indices = torch.cat((all_indices, indices), dim=0)
    # print('all_outputs in the end ', all_outputs)

    result_dict = {
        'all_outputs': all_outputs,

    }
    all_outputs_numpy = all_outputs.numpy()

    np.save('/data/temp/all_outputs_numpy', all_outputs_numpy)


    return result_dict


def save_inference_results_on_disk(loader, network, name):
    config = loader.config
    pack_volume = config['pack_volume']
    path = os.path.join(config['temp_folder'], name, '')
    print('path ', path)
    network.eval()
    network = network.cuda()
    with torch.cuda.device(config['gpu']):
        all_outputs = torch.cuda.FloatTensor()
        i = 1
        print('Inference is in progress')
        print('loader ', loader.batch_sampler.sampler)
        print('len(loader)', len(loader))
        for data in tqdm(loader):
            images, labels, indices = data

            if config['2head']:
                outputs, for_softmax = network(Variable(images).cuda())
            else:
                outputs = network(Variable(images).cuda())

            if pack_volume != 1:
                all_outputs = torch.cat((all_outputs, outputs.data), dim=0)
            else:
                all_outputs = outputs

            if i % pack_volume == 0:
                torch.save(all_outputs, '%sall_outputs_%d' % (path, i))
                all_outputs = torch.cuda.FloatTensor()
                torch.cuda.empty_cache()
            i += 1
        batches_number = len(loader) // pack_volume
        print('len(loader) ', len(loader))
        all_outputs = None
        torch.cuda.empty_cache()
        return batches_number


def load_inference(loader, batches_number_real=True):
    print('Loading the inference from disk...', datetime.datetime.now())
    config = loader.config
    pack_volume = config['pack_volume']
    if batches_number_real:
        batches_number = len(loader) // pack_volume
    else:
        batches_number = 2
    result_dict = read_inference_results_from_disk(config,
                                                   batches_number,
                                                   loader.dataset.name)
    all_outputs = \
        result_dict['all_outputs']
    print('Loading the inference from disk is finished!', datetime.datetime.now())
    result_dict = {
        'all_outputs': all_outputs,
    }
    return result_dict


def get_cosine_similarities_matrix(representation_outputs_1, representation_outputs_2, config):
    x = representation_outputs_1.cpu().numpy()
    y = representation_outputs_2.cpu().numpy()
    matrix = cosine_similarity(x, y)
    with torch.cuda.device(config['gpu']):
        return torch.from_numpy(matrix).cuda().float()


def get_signs_matrix(config, labels_1, labels_2, negative_sign=0, positive_sign=1):
    """

    :param config: confi gobject to get gpu device from
    :param labels_1: CUDA LongTensor of size [number_of_labels, 1]
    :param labels_2: CUDA LongTensor of size [number_of_labels, 1]
    :param negative_sign: 0 or -1
    In MAP evaluation and 0 is convinient.
    In loss functions -1 is required.
    :return:
    """
    all_labels_1 = labels_1.cpu().numpy()
    all_labels_2 = labels_2.cpu().numpy()
    if negative_sign == 0:
        matrix = np.zeros((len(labels_1), len(labels_2)), dtype=int)
    if negative_sign == -1:
        matrix = -np.ones((len(labels_1), len(labels_2)), dtype=int)
    if negative_sign == 1:
        matrix = np.ones((len(labels_1), len(labels_2)), dtype=int)
    for i, current_label in enumerate(all_labels_1):
        matrix[i, np.where(all_labels_2 == current_label)[0]] = positive_sign
    if negative_sign == 0:
        print('percentage of positives in matrix ', np.mean(matrix))
    with torch.cuda.device(config['gpu']):
        matrix = torch.from_numpy(matrix)  # .cuda()
        return matrix


def get_signs_matrix_row(query, all_labels):
    matrix_row = np.zeros((1, len(all_labels)), dtype=int)
    matrix_row[0, np.where(all_labels == all_labels[query])[0]] = 1
    return matrix_row


def freeze_layers(config, pretrained_resnet):
    # Freeze all layers
    for i, param in pretrained_resnet.named_parameters():
        param.requires_grad = False
    # Unfreeze the tail of the net starting from config['freeze_in_pre_classification']['layer_name']
    frozen_layers = []
    for name, child in pretrained_resnet.named_children():
        print('name ', name)
        if config['freeze_in_pre_classification']['layer_name'] in frozen_layers:
            print('name for unfreeze ', name)
            for params in child.parameters():
                params.requires_grad = True
        frozen_layers.append(name)

    return pretrained_resnet


def print_layers_freeze_status(pretrained_net):
    # To view which layers are freeze and which layers are not frozen:
    for name, child in pretrained_net.named_children():
        print('-' * 30, name, '-' * 30)
        for name_2, params in child.named_parameters():
            print(name_2, params.requires_grad)


def check_configuration(config):
    if config['sampling']['stage'] == 'loss':
        assert config['sampling']['strategy'] in ['none', 'all', 'equal']
    if config['sampling']['stage'] == 'batch':
        assert config['sampling']['strategy'] in ['none', 'uniform']


def get_loaders(config):
    # These loaders may require specific sampling strategy
    train_loader = Landmark2019DataLoader(config, name='train')
    print('')
    index_loader = Landmark2019DataLoader(config, name='index')
    print('')
    test_loader = Landmark2019DataLoader(config, name='test')
    print('')

    # These loaders has smaller batch size to evaluate on the whole dataset
    train_loader_for_evaluation = Landmark2019DataLoader(config, name='train', init=True)
    print('')

    results_dict = {
        'test_loader': test_loader,

        'train_loader': train_loader,
        'train_loader_for_evaluation': train_loader_for_evaluation,
        'index_loader': index_loader,

    }
    return results_dict
