import argparse
import logging
import torch
from torch import nn
import os
import jstyleson
from torchvision import models
from data_loader.data_loaders import Landmark2019DataLoader
from logger.logger import Logger

from results.knn import nearest_neighbors
from model.model import Landmark2019Model
from model.loss import classification_loss, histogram_loss, cosine_loss, triplet_loss, contrastive_loss, center_loss

from trainer.classification_trainer import ClassificationTrainer
from trainer.representation_trainer import RepresentationTrainer
from utils.util import freeze_layers, check_configuration, get_loaders, print_layers_freeze_status

import pretrainedmodels

logging.basicConfig(level=logging.INFO, format='')


def main(config, resume):
    train_logger = Logger()

    check_configuration(config)

    pretrained_net = classification_pretraining(config, resume, train_logger)

    loaders_dict = get_loaders(config)
    test_loader = loaders_dict['test_loader']
    train_loader = loaders_dict['train_loader']
    train_loader_for_evaluation = loaders_dict['train_loader_for_evaluation']
    index_loader = loaders_dict['index_loader']

    model = representation_training(config, pretrained_net, resume, train_loader, train_loader_for_evaluation,
                                    train_logger)

    # get nearest neighbors
    nearest_neighbors(model, loader_index=index_loader, loader_test=test_loader, k=100,
                      inference='do')


def classification_pretraining(config, resume, train_logger):
    # These loader requires no sampling strategy
    train_loader_for_classification = Landmark2019DataLoader(config, name='train', for_classification=True)

    train_classes_number = train_loader_for_classification.dataset.classes_number
    with torch.cuda.device(config['gpu']):
        pretrained_net = pretrainedmodels.se_resnext50_32x4d()  # pretrainedmodels.densenet121()
        # #pretrainedmodels.se_resnext50_32x4d()
        # pretrainedmodels.se_resnext101_32x4d()
        # models.resnet50(pretrained=True)
        print('pretrained_net', pretrained_net)
        # input()

        print('train_classes_number ', train_classes_number)
        # for resnet50
        # pretrained_net.fc = nn.Linear(pretrained_net.fc.in_features, train_classes_number)

        # for se_resnext50_32x4d or densenet121()
        pretrained_net.last_linear = nn.Linear(pretrained_net.last_linear.in_features, train_classes_number)

        print(pretrained_net)


        if config['freeze_in_pre_classification']['do']:
            pretrained_net = freeze_layers(config, pretrained_net)  # for se_resnext50_32x4d
            #pretrained_net.features = freeze_layers(config, pretrained_net.features) # for densenet121()

        if not config['classification_pretraining']:
            print('Loading the basic net already pretrained on our data from %s' % config['pretrained_resnet_file'])
            checkpoint_for_resnet = torch.load(config['pretrained_resnet_file'])

            print(checkpoint_for_resnet['state_dict'].keys())
            pretrained_net.load_state_dict(checkpoint_for_resnet['state_dict'])

        else:
            print('Classification pretraining is in progress...')
            classification_trainer = ClassificationTrainer(
                pretrained_net,
                loss=eval(config['classification_loss']),
                metrics=[],  # [eval(config['metrics']['metric_for_pre_classification'])],
                resume=resume,
                config=config,
                train_loader=train_loader_for_classification,
                test_loader=train_loader_for_classification,
                # here we cannot test on the test set
                # because the number of classes in the test set is different
                # For our task the classification plays the role of better initialization
                # so we can just do it for the few epochs and do not care much about classification quality
                train_logger=train_logger
            )

            classification_trainer.train()
            torch.save({'state_dict': pretrained_net.state_dict()}, config['pretrained_resnet_file'])

        print('Classification pretraining is done!')
        print('pretrained_net ', pretrained_net)
        return pretrained_net


def representation_training(config, pretrained_net, resume, train_loader, train_loader_for_evaluation, train_logger):
    print_layers_freeze_status(pretrained_net)
    model = Landmark2019Model(config=config, loader_for_pca_initialization=train_loader_for_evaluation,
                              basic_net=pretrained_net)
    model.summary()

    trainer = RepresentationTrainer(model, loss=eval(config['loss']['name']), metrics=[], resume=resume, config=config,
                                    train_loader=train_loader, test_loader=None, train_logger=train_logger)
    trainer.train()

    return model


if __name__ == '__main__':
    logger = logging.getLogger()

    parser = argparse.ArgumentParser(description='Landmark2019')
    parser.add_argument('-c', '--config', default=None, type=str,
                        help='config file path (default: None)')
    parser.add_argument('-r', '--resume', default=None, type=str,
                        help='path to latest checkpoint (default: None)')

    args = parser.parse_args()

    config = None
    if args.resume is not None:
        # if args.config is not None:
        #     logger.warning('Warning: --config overridden by --resume')
        # config = torch.load(args.resume)['config']
        config = jstyleson.load(open(args.config))
    elif args.config is not None:
        config = jstyleson.load(open(args.config))
        path = os.path.join(config['trainer']['save_dir'], config['name'])
        path_class = os.path.join(config['pre_classification_trainer']['save_dir'], config['name'])
        # assert not os.path.exists(path), 'Path {} already exists!'.format(path)
    assert config is not None

    main(config, args.resume)
