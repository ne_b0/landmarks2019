import torch

from improvement.db_aug import index_set_aug
from results.metric import accuracy, get_nearest_neighbors, get_map_at_k
from improvement.reranking import expansion_and_reranking
from utils.util import outputs_for_large_dataset, load_inference
import numpy as np


def predict(index_labels, i, s, version='simple'):
    array_of_nearest_neighbors_classes = index_labels[i]
    if version == 'simple':
        return array_of_nearest_neighbors_classes[:, 0]  # the simplest version for now
    if version == 'learn':
        return 42  # this is just a placeholder for more sophisticated classification


def get_accuracy(queries_labels, index_labels, i, s, version):
    predictions = predict(index_labels, i, s, version)
    print('Final accuracy = ', accuracy(queries_labels, predictions))


def nearest_neighbors(model, loader_index, loader_test, k, inference='load'):
    config = loader_index.config
    with torch.cuda.device(config['gpu']):
        if inference == 'do':
            result_dict_index = outputs_for_large_dataset(loader_index, network=model)
            result_dict_test = outputs_for_large_dataset(loader_test, network=model)
        else:
            print('Before loading the inference loader ', loader_index.dataset.name)
            result_dict_index = load_inference(loader_index)
            result_dict_test = load_inference(loader_test)

        all_outputs_index = result_dict_index['all_outputs']
        all_outputs_test = result_dict_test['all_outputs']

        print('Results without expansion/reranking:')
        print('all_outputs ', all_outputs_index.shape)
        # neighbors_similarities, neighbors_indices = get_nearest_neighbors(
        #     queries=all_outputs_test,
        #     index_set=all_outputs_index,
        #     k=k)
        neighbors_similarities, neighbors_indices = index_set_aug(queries=all_outputs_test,
                                                                  index_set=all_outputs_index)
        # if config['expansion-reranking']:
        #     print('Results after expansion/reranking:')
        #     neighbors_indices, neighbors_similarities = expansion_and_reranking(
        #         neighbors_indices,
        #         neighbors_similarities,
        #         index_set=all_outputs_index,
        #         times=1,
        #         k=k)
        np.save('100_nearest_neighbors', neighbors_indices)
        np.save('100_nearest_neighbors_similarities', neighbors_similarities)
