from data_loader.data_loaders import Landmark2019DataLoader
from model.model import Landmark2019Model
from results.metric import map_test_for_representation
from utils.util import outputs_for_large_dataset, load_inference


def evaluate_without_learning(config, pretrained_net, stage=''):
    # These loaders has smaller batch size to evaluate on the whole dataset
    train_loader_for_evaluation = Landmark2019DataLoader(config, name='train', init=True)
    test_loader_for_evaluation = Landmark2019DataLoader(config, name='test', init=True)
    model = Landmark2019Model(config=config, loader_for_pca_initialization=train_loader_for_evaluation,
                              basic_net=pretrained_net)
    print('*' * 100)
    print('*' * 100)
    print('*' * 100)
    print('Evaluation on train set before %s' % stage)
    evaluate_map_on_dataset(k=config['metrics']['k_for_MAP'], loader=train_loader_for_evaluation,
                            network=model,
                            inference='do')
    knn(pretrained_net, train_loader_for_evaluation, k=1)
    print('*' * 100)
    print('Evaluation on test set before %s' % stage)
    evaluate_map_on_dataset(k=config['metrics']['k_for_MAP'], loader=test_loader_for_evaluation,
                            network=model,
                            inference='do')
    knn(pretrained_net, test_loader_for_evaluation, k=1)
    print('*' * 100)
    print('*' * 100)
    print('*' * 100)


def evaluate_map_on_dataset(k, loader, network, inference='do'):
    if inference == 'do':
        network.eval()
        result_dict = outputs_for_large_dataset(loader, network=network)
    else:
        result_dict = load_inference(loader)
    all_outputs, all_labels = result_dict['all_outputs'], result_dict['all_labels']

    return map_test_for_representation(k, all_outputs, all_labels, loader)
