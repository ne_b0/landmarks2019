import os

import numpy as np
import torch
from sklearn.metrics import accuracy_score
from tqdm import tqdm

from utils.util import get_signs_matrix_row

import faiss


#######################################################################################################################
#
# Metric for representation
#
#######################################################################################################################

# Be careful in case you queries = index_set:
# This function returns the query itself as the nearest neighbor
def get_nearest_neighbors(queries, index_set, k, distance_type='cosine_similarity'):
    dimension = queries.shape[1]
    queries = queries.cpu().numpy().astype('float32')
    index_set = index_set.cpu().numpy().astype('float32')

    # resources = [faiss.StandardGpuResources() for j in range(faiss.get_num_gpus())]
    # resource = resources[1]

    resource = faiss.StandardGpuResources()
    if distance_type == 'cosine_similarity':
        queries_number = queries.shape[0]
        index_to_test = np.random.randint(low=0, high=queries_number)
        assert np.abs(np.linalg.norm(queries[index_to_test]) - 1.0) <= 10e-1, \
            'Cosine similarity nearest neighbors search should work with normalized data! ' \
            'But np.linalg.norm(queries[%d] = %.10e' % (index_to_test, np.linalg.norm(queries[index_to_test]))
        index = faiss.GpuIndexFlatIP(resource, dimension)
    else:
        index = faiss.GpuIndexFlatL2(resource, dimension)

    index.add(index_set)
    neighbors_similarities, neighbors_indices = index.search(queries, k)

    return neighbors_similarities, neighbors_indices


# http://web.stanford.edu/class/cs276/handouts/EvaluationNew-handout-6-per.pdf
def get_map_at_k(all_labels, nearest_neighbors):
    map_at_k = 0.0
    k = nearest_neighbors.shape[1]
    queries_number = all_labels.shape[0]

    for query in tqdm(range(queries_number)):
        nearest_neighbors_indices = np.asarray(nearest_neighbors[query], dtype=int)
        signs_matrix_row = get_signs_matrix_row(query, all_labels)
        nearest_neighbors_signs = signs_matrix_row[0, nearest_neighbors_indices]

        cumsum = np.cumsum(nearest_neighbors_signs, axis=0)
        thresholds = np.arange(start=1, stop=k + 1)
        # only for relevant items - so we multiply by nearest_neighbors_signs using it like a mask
        precision_at_threshold = (cumsum * nearest_neighbors_signs)[:k] / thresholds
        average_precision = np.sum(precision_at_threshold)

        # divide only on the number of relevant examples if it is not zero
        relevant_items_number = cumsum[k - 1]
        if relevant_items_number > 0.0:
            map_at_k += average_precision / relevant_items_number

    map_at_k = map_at_k / queries_number
    return map_at_k


def map_test_for_representation(k, all_outputs, all_labels, loader):
    config = loader.config
    with torch.cuda.device(config['gpu']):
        s, i = get_nearest_neighbors(all_outputs, all_outputs, k + 1)
        i = i[:, 1:]  # we don't want the item itself to be taken into account
        if loader.config['visualization']['images_visualization']:
            torch.save(i, os.path.join(config['temp_folder'], 'nearest_neighbors_%s' % loader.dataset.name))

        map_at_k = get_map_at_k(all_labels, i)
        print('MAP_at_', k, ' of the network: %f ' % map_at_k)
        all_outputs = None
        all_labels = None
        torch.cuda.empty_cache()
        return map_at_k


#######################################################################################################################
#
# Metric for classification
#
#######################################################################################################################


def accuracy(inp, target):
    return accuracy_score(y_pred=inp, y_true=target)
