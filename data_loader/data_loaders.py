import os

from torch.utils.data import DataLoader
from torch.utils.data.sampler import BatchSampler, RandomSampler, SequentialSampler

from data_loader.sampling import UniformSampler
from data_sets import landmark2019_dataset


class Landmark2019DataLoader(DataLoader):
    """
    Landmark2019 data loading
    """

    def __init__(self, config, name, init=None, for_classification=False):
        super(Landmark2019DataLoader, self).__init__(
            dataset=landmark2019_dataset.Landmark2019(data_folder=os.path.join(config['data_loader']['data_dir'], name),
                                                      config=config, name=name),
            batch_size=config['data_loader']['batch_size_init' if init else 'batch_size_%s' % name],
            drop_last=config['data_loader']['drop_last']
        )
        if config['sampling']['strategy'] == 'uniform' and not (for_classification or init) and name == 'train':
            if config['2head']:
                print('self.dataset ', self.dataset, self.dataset.__len__())
                random_sampler = RandomSampler(self.dataset)
                print('random_sampler ', random_sampler, random_sampler.num_samples)
                self.batch_sampler = BatchSampler(
                    random_sampler,
                    batch_size=self.batch_size,
                    drop_last=self.drop_last
                )
            else:
                number_of_different_classes_in_batch = 2
                batches_number = self.dataset.__len__() * number_of_different_classes_in_batch // self.batch_size

                uniform_sampler = UniformSampler(
                    data_source=self.dataset,
                    batches_number=batches_number,
                    number_of_different_classes_in_batch=number_of_different_classes_in_batch,
                    batch_size=self.batch_size
                )
                self.batch_sampler = BatchSampler(
                    uniform_sampler,
                    batch_size=self.batch_size,
                    drop_last=self.drop_last
                )
        else:
            if name == 'train':
                print('self.dataset ', self.dataset, self.dataset.__len__())
                random_sampler = RandomSampler(self.dataset)
                print('random_sampler ', random_sampler, random_sampler.num_samples)
                self.batch_sampler = BatchSampler(
                    random_sampler,
                    batch_size=self.batch_size,
                    drop_last=self.drop_last
                )
            else:
                sequential_batch_sampler = SequentialSampler(self.dataset)
                print('sequential_batch_sampler ', sequential_batch_sampler.data_source)
                self.batch_sampler = BatchSampler(
                    sequential_batch_sampler,
                    batch_size=self.batch_size,
                    drop_last=False,
                )

        self.config = config
