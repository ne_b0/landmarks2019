import torch

import numpy as np
from torch.utils.data.sampler import Sampler


class UniformSampler(Sampler):
    """Samples elements with roughly uniform distribution of samples with the same label
    Arguments:
    """

    def __init__(self, data_source,
                 batch_size,
                 number_of_different_classes_in_batch,
                 batches_number):
        """

        :param data_source: dataset, should be an inheritor of Dataset
        :param batch_size: desired batch size, int
        :param number_of_different_classes_in_batch: desired number of different classes in batch,usually 2 or 3
        :param batches_number: how many batches you want to create
        """
        super().__init__(data_source)
        self.data_source = data_source
        self.labels = self.data_source.labels
        self.length = self.labels.shape[0]  # how many samples we have in our dataset
        self.number_of_samples_with_the_same_label_in_the_batch = batch_size // number_of_different_classes_in_batch
        self.number_of_different_classes_in_batch = number_of_different_classes_in_batch
        self.batches_number = batches_number

        print('self.number_of_samples_with_the_same_label_in_the_batch ',
              self.number_of_samples_with_the_same_label_in_the_batch)
        print('self.number_of_different_classes_in_batch ', self.number_of_different_classes_in_batch)
        print('self.batches_number ', self.batches_number)

    def draw_samples_with_label(self, label):
        selected_samples = np.where(self.labels == label)[0]

        # in case we have too few sample with this label we just duplicate examples
        total_selected_samples = selected_samples
        # print('total_selected_samples ', total_selected_samples.shape[0])
        while selected_samples.shape[0] < self.number_of_samples_with_the_same_label_in_the_batch:
            selected_samples = np.hstack((total_selected_samples, selected_samples))

        # shuffle
        samples = np.random.permutation(selected_samples)
        # take the requested number of samples
        samples = samples[:self.number_of_samples_with_the_same_label_in_the_batch]
        return samples

    def get_new_batch(self):
        batch = np.array([], dtype=int)
        labels_already_in_batch = []
        for class_number in range(self.number_of_different_classes_in_batch - 1):
            label = np.random.choice(np.array(self.labels))
            # print('label ', label)
            while label in labels_already_in_batch:
                label = np.random.choice(np.array(self.labels))
            labels_already_in_batch.append(label)
            batch = np.hstack((batch, self.draw_samples_with_label(label)))
        # print('batch ', batch)
        return batch

    def __iter__(self):
        batches = np.array([], dtype=int)
        for batch_number in range(self.batches_number):
            new_batch = self.get_new_batch()
            batches = np.hstack((batches, new_batch))
        return iter(np.array(batches))

    def __len__(self):
        return self.batches_number * \
               self.number_of_different_classes_in_batch * \
               self.number_of_samples_with_the_same_label_in_the_batch


#######################################################################################################################
#
# For sampling on the loss stage
#
#######################################################################################################################


def get_indices_for_equal_sampling(config, signs_matrix, negative_pair_sign=0):
    signs_matrix = signs_matrix.cpu().numpy()
    with torch.cuda.device(config['gpu']):
        # if we have at least one positive pair
        if 1 in signs_matrix:
            positive_pairs_indices = list(np.where(signs_matrix == 1))
            negative_pairs_indices = list(np.where(signs_matrix == negative_pair_sign))

            positive_pairs_number = (positive_pairs_indices[0]).shape[0]
            negative_pairs_number = (negative_pairs_indices[0]).shape[0]

            if negative_pairs_number >= positive_pairs_number:
                permutation = np.random.permutation(negative_pairs_number)
                negative_pairs_indices[0] = negative_pairs_indices[0][permutation]
                negative_pairs_indices[0] = negative_pairs_indices[0][:positive_pairs_number]
                negative_pairs_indices[1] = negative_pairs_indices[1][permutation]
                negative_pairs_indices[1] = negative_pairs_indices[1][:positive_pairs_number]

            indices_for_sampling_0 = np.hstack((positive_pairs_indices[0], negative_pairs_indices[0]))
            indices_for_sampling_1 = np.hstack((positive_pairs_indices[1], negative_pairs_indices[1]))
            indices_for_sampling = [indices_for_sampling_0, indices_for_sampling_1]

            # for histogram loss we should return the matrix with 1 for taken example and 0 for not taken
            if config['loss']['name'] == 'histogram_loss':
                s_indices = np.zeros(signs_matrix.shape, dtype=int)
                s_indices[indices_for_sampling] = 1
                indices_for_sampling = s_indices
        else:
            indices_for_sampling = None
    return indices_for_sampling
